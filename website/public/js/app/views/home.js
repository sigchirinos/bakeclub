/**
    The main dashboard view of the application.
    @module app/views/home
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/views/slider/slider',
    'app/views/streamer/streamer',
    'app/collections/stream',
    'app/models/note',
    'app/models/tweet',
    'app/models/comment',
    'app/models/media',
    'text!app/tpl/home.html'
    ],
function(BaseView, SliderView, StreamerView, StreamColl, Note, Tweet, Comment, Media, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias module:app/views/home
     */
    var exports = BaseView.extend({
        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // Compile template
            this.template = _.template(Tpl);

            // Init the stream collections that the stream view will use
            var collections = [];
            collections['note'] = new StreamColl([], {
                url : '/api/notes',
                model : Note
            });
            collections['tweet'] = new StreamColl([], {
                url : '/api/notes/tweets',
                model : Tweet
            });
            collections['talk'] = new StreamColl([], {
                url : '/api/comments',
                model : Comment
            });
            collections['media'] = new StreamColl([], {
                url : '/api/media',
                model : Media
            });

            // Create a streamer view with the array
            // of collections we just initialized
            var streamer = new StreamerView({
                collections : collections,
                source : '1'
            });

            // Add child views
            this.views['#main-right'] = streamer;
            this.views['#main-left'] = new SliderView();
            
        }
    });

    return exports;
});