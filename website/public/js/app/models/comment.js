/**
 *  Handles CRUD requests for an individual comment
 *  @module app/models/comment
 *  @extends Backbone.Model
 */
define([
	'backbone/backbone'
], 
function(Backbone){
	/**
	 * @constructor
	 */
	var exports = Backbone.Model.extend({
	    parse : function(response) {
	        // Check for "data" property, means we are getting it back directly from server
	        if(response.data) {
	            var data = response.data;
	        } else {
	            var data = response;
	        }

	        // Create client unique identifier
	        data.client_ident = "comment" + data.id;

	        return data;
	    }
	});

	return exports;
});