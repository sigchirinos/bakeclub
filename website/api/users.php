<?php 
// Bootstrap application
require 'bootstrap.php';

// We use this object later as route middleware
// to implement roles based access control
$acl = new \Bake\Acl();

// Create swagger doc object
$apiDoc = new \Slagger\APIDoc('/users/api-doc/users', [
      'resourcePath' => '/users',
      "models" => []
]);

// Inject swagger documentation generation middleware
$app->add($apiDoc);

//---------------------------------
// Create a new user
//---------------------------------
$app->post('/users', $apiDoc->routeDoc('createuser', [

      // Swagger doc
      "summary" => "Create a user",
      "POST" => [
            ["name" => "username", "description" => "The username of the new user"],
            ["name" => "password", "description" => "The password for the new user"],
            ["name" => "fname", "description" => "First name for the user"],
            ["name" => "lname", "description" => "Last name for the user"],
            ["name" => "email", "description" => "Email address for the user"],
            ["name" => "about", "description" => "Short blurb about the user"]
      ]

]), function () use ($app) {

	// Init User object
	$user = new \Bake\User();
      
	// Create a new user
	$user_info = $user->create([
            'username' => $app->request()->post('username'),
            'password' => $app->request()->post('password'),
            'fname' => $app->request()->post('fname'),
            'lname' => $app->request()->post('lname'),
            'email' => $app->request()->post('email'),
            'about' => $app->request()->post('about')
      ]);     

	// Send user info back to user
	echo json_encode($user_info);

})->name('createuser');

//---------------------------------
// Start application
//---------------------------------
$app->run();