<?php 
/**
 * Database connection cache registry 
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Db_Cache {
    
    /*
    * Private: $_instance
    * Type: Object
    * Stores instance of the object
    */
    private static $_instance = null;
    
    /*
    * Private: $_conninfo
    * Type: Array
    * Array of database connection info
    */
    private $_connInfo = array();
    
    /*
    * Private: $_currentServer
    * Type: String
    * Key name of current default server
    */
    private $_currentServer;
    
    /*
    * Private: $_servers
    * Type: Array
    * Array of database connection objects
    */
    private $_servers = array();
 
    /**
     * Private constructor, use getInstance method instead
     * 
     * @return void  
     */
    private function __construct($dbMap, $key = "") {

        // store connection strings in registry
        $this->_connInfo = $dbMap;
        
        // set default array key if not specified
        if (empty($key)){
            $key = key($this->_connInfo);
        }
        
        // connect to default database (first in connection info array)
        $conn = BK_Db_Conn::open($this->_connInfo[$key]["host"], $this->_connInfo[$key]["user"], $this->_connInfo[$key]["password"], $this->_connInfo[$key]["dbname"]);
        
        // check connection
        if ($conn !== false) {
            // store default server registry key
            $this->_currentServer = $key;
                
            // store connection object
            $this->_servers[$key] = $conn;
        } else {
            throw new Exception("Could not connect to default database.");
        }
    }
 
    /**
     * Clone object, restricted in singleton pattern
     * 
     * @return void  
     */
    protected function __clone() {
        // restricts cloning of the object
    }
    
    /**
     * Authenticates and connects to a server
     * 
     * @param string $key name of the server to connect to
     * 
     * @return void  
     */
    private function connectServer($key) {
        // get connection information
        if (array_key_exists($key, $this->_connInfo)) {
            // connect to server
            return BK_Db_Conn::open($this->_connInfo[$key]["host"], $this->_connInfo[$key]["user"], $this->_connInfo[$key]["password"], $this->_connInfo[$key]["dbname"]);
        } else {
            throw new Exception('Database connection information not found.');
        }
    }
    
    /**
     * instantiates instance of object
     * 
     * @param string $key name of the server to initially connect to
     * 
     * @return void
     */
    public static function getInstance($dbMap, $key = "") {
        // check if object already instantiated
        if (is_null(self::$_instance)) {
            self::$_instance = new self($dbMap, $key);
        }
        
        // return instance
        return self::$_instance;
    }
    
    /**
     * retrieves server connection object
     * 
     * @param string $key name of the server connection to retrieve
     * 
     * @return void
     */
    public function getConnection($key = "") {
        // connect to default database if key not specified
        if (empty($key)) {
            // return first connection object
            return $this->_servers[$this->_currentServer];
        }
        
        // check for existence of connection in registry
        if (array_key_exists($key, $this->_servers)) {
            return $this->_servers[$key];
        } else {
            // connect server and store in registry
            $this->setConnection($key, $this->connectServer($key));
            
            // return connection
            return $this->_servers[$key];
        }
    }
    
    /**
     * adds a new server connection to registry
     * 
     * @param string $key name to use for the connection we are storing
     * @param string $conn the connection object to store
     * 
     * @return void
     */
    public function setConnection($key, $conn) {
        $this->_servers[$key] = $conn;
    }
    
    /**
     * set current default connection object
     * 
     * @param string $key name of the server to set current
     * 
     * @return void
     */
    public function setCurrentServer($key) {
        // check if server connection exists
        if (array_key_exists($key, $this->_servers)) {
            // set as default server
            $this->_currentServer = $key;
        } else {
            // connect server and store in registry
            $this->setConnection($key, $this->connectServer($key));
            
            // set as default server
            $this->_currentServer = $key;
        }
    }
}