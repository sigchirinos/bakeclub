/**
    Provides a slider control to view things in a series.
    @module app/views/slider/slider
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/views/slider/indicator',
    'app/views/slider/eventslide',
    'cubiq/iscroll',
    'text!app/tpl/slider/slider.html'
    ],
function(BaseView, SlideIndicator, EventSlide, IScroll, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias app/views/slider/slider
     */
    var exports = BaseView.extend({
        /**
            HTML tag to use for the view's root element.
            @type {String}
            @default
         */
        tagName : 'div',

        /**
            HTML class for the view's root element.
            @type {String}
            @default
         */
        className : 'slider-widget',

        /**
            Reference to the iScroll widget
            @type {iScroll}
         */
        iscroll : {},

        /**
            Reference to the iScroll position indicator
            @type {app/slider/indicator}
         */
        indicator : {},

        /**
            Stores a reference to the collection object that feeds slide information from the server
            @type {app/views/streamer/streamer}
         */
        collection : {},

        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // Compile template
            this.template = _.template(Tpl);

            // Listen to when new models are added to collection
            // so we can render the item when its added
            this.collection.on("add", this.addItem, this);
            this.collection.on("reset", this.addMany, this);
        },

        /**
         * Add multiple events to the event slider view
         */
        addMany : function() {
            // Iterate through the entire collection and add each as a slide
            this.collection.each(this.addItem, this);
        },

        /**
         * Add new event to the event slider view
         * @param {app/models/event} item the event to add to the slider
         */
        addItem : function(item) {
            // Create new event slide view, with model set to passed Event model 
            var view = new EventSlide({
                model : item
            });

            // Attach reference of this object to the child slide 
            view.parentSlider = this;

            // render the view and insert into slider
            this.$('#slide-cont .slider').append(view.render().el);

            // add new indicator position for item
            this.indicator.add(this.iscroll.pagesX.length, item);

            // refresh slider control
            this.iscroll.refresh();
        },

        /**
         * Called after the main rendering logic takes place
         */
        postRender : function() {
            // Now that template is added to DOM we can add the iscroll control
            this.iscroll = new iScroll($('#slide-cont', this.$el)[0], {
                snap : 'li',
                momentum : false,
                hScrollbar : false,
                vScroll : false,
                onScrollEnd : function() {
                    return function () {
                        $('.indicator ul li.active').removeClass('active');
                        $('.indicator ul > li:nth-child(' + (this.currPageX + 1) + ')').addClass('active');
                    };
                    
                }()
            });

            // Create slide indicator
            this.indicator = new SlideIndicator();

            // Pass reference to slider to the indicator
            this.indicator.slider = this.iscroll;

            // Get data from server to trigger filling of iscroll
            this.collection.fetch();
        
            // render indicator and display
            $('#indicator-wrap', this.$el).prepend(this.indicator.render().el);
        }

    });

    return exports;
});