/** 
    Login view
    @module app/views/login
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/helpers/dispatcher',
    'text!app/tpl/login.html'
    ],
function(BaseView, Dispatch, Tpl){
    /**
     * @constructor
     * @alias module:app/views/login
     */
    var exports = BaseView.extend({
        /**
         * Specifies the event handlers and which functions the wire up to.
         * @type {Object}
         */
        events : {
            "click .btn-primary"            : "login",
            "click .login-link"             : "showLoginModal",
            "click .logout-link"            : "logout",
            "keypress input[name=password]" : "authOnEnter"
        },

        /**
         * Init stuff
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // Compile template
            this.template = _.template(Tpl);

            // Register event to show the myaccount dropdown whenever login is successful
            Dispatch.on("login:success", this.showMyAcc, this);
        },

        /**
         * Display the login modal
         */
        showLoginModal : function(e) {
            // Stop bubbling
            e.preventDefault();

            // Show modal
            $('#loginModal', this.$el).modal('show');

            // set focus to username box when modal shows
            var self = this;
            $('#loginModal', this.$el).on('shown', function () {
                $("input[name='username']", self.$el).focus();
            });
            
        },

        /**
         * Handles login click
         */
        login : function(e) {
            // Stop bubbling
            e.preventDefault();

            // Authenticate user
            this.auth();
        },

        /**
         * Display the login modal
         */
        logout : function(e) {
            // Stop bubbling
            e.preventDefault();

            // referenc to this
            var self = this;

            // Call service to logout
             $.ajax('/api/auth/logout', {
                success: function () {
                    // Clear logged in settings
                    localStorage.removeItem('loggedIn');
                    localStorage.removeItem('roles');

                    // Fire logout event
                    Dispatch.trigger("logout", true);

                    // Show login link
                    $('.login-link', self.$el).show();

                    // Hide dropdown
                    $('.dropdown', self.$el).hide();
                },
                error: function () {

                }
             });
        },

        /**
         * Display the My account dropdown
         */
        showMyAcc : function() {
            // Hide login link in header
            $('.login-link', this.$el).hide();

            // Show My Account drop down
            $('.dropdown', this.$el).show();

            // Hide modal
            $('#loginModal', this.$el).modal('hide');
        },

        /**
         * Checks keypress to see if enter was pressed and then does authorization
         */
        authOnEnter : function(e) {
            if (e.keyCode === 13) {
                this.auth();
            }
        },

        /**
         * Login a user
         */
        auth : function() {
            // Hide alert box
            $('.alert-error', this.$el).hide();

            // Get username and password
            var username = $("input[name='username']", this.$el).val();
            var password = $("input[name='password']", this.$el).val();
           
            // Store reference to this for closures           
            var self = this;

            // Call authenication api
            $.ajax('/api/auth/login', {
                type: "post", 
                data: {"username": username, "password": password},
                success: function (response){
                    // Clear boxes
                    $("input[name='username']", this.$el).val('');
                    $("input[name='password']", this.$el).val('');

                    // Store app state info
                    localStorage['loggedIn'] = 1; 
                    if (response.data && response.data.roles) {
                        localStorage['roles'] = JSON.stringify(response.data.roles);
                    }

                    // Fire login event
                    Dispatch.trigger("login:success", "success");

                    // Show my account dropdown
                    self.showMyAcc();
                }, 

                error: function (response){
                    // Error response body is json
                    var result = $.parseJSON(response.responseText);

                    // Select password text, for ease of typing in again
                    $("input[name='password']", this.$el).select();

                    // Show error in alert box
                    if (result && result.meta && result.meta.msg) {
                        $('.alert-error', self.$el).html(result.meta.msg);
                    } else {
                        $('.alert-error', self.$el).html('Unknow error');
                    }

                    // Show alert box
                    $('.alert-error', self.$el).show();
                }
            });
            
        },

        /**
         * Render the view's template HTML and initializes child views
         *
         * @returns this
         */
        render : function() {
            // Call parent
            BaseView.prototype.render.call(this);

            // Check logged in status
            if (localStorage && localStorage.loggedIn && localStorage.loggedIn === "1") {
                // Show my account dropdown
                this.showMyAcc();
            }

            // For chaining
            return this;
        }

    });

    return exports;
});