<?php 
/**
 * This class manages execution of queries and access to the database. 
 * It helps manage the connection pool to allow you to chose which database to run a query on.
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Db_Query {
    
    /**
     * Database connection object
     * @var mixed
     */
    public $dbconn;
    
    /**
     * Map of database connection information
     * @var array
     */
    public $dbMap;
    
    /**
     * Flag to tell whether to print debug info
     * @var bool
     */  
    public $debug = false;
    
    /**
     * The kind of db connection to make.
     * @var string
     */ 
    public $type = "mysqli";
    
    /**
     * Determines whether store_result is called on mysqli statement
     * @var bool
     */
    public $storeresult = false;
    
    /**
     * Determines whether to use fully qualified names for returning result columns
     * @var bool
     */
    public $fqnames = false;
    
    /**
     * Determines which database to connect to
     * @var bool
     */
    public $connecto = "";

    /**
     * Message info from a query that executed, error messages, etc...
     * @var string
     */
    public $msg;
    
    /**
     * Returned row data of the query
     * @var array
     */
    public $rows = array();
    
    /**
     * Number of rows returned
     * @var array
     */
    public $rowcount;
    
    /**
     * Number of rows affected for insert,delete statements
     * @var array
     */
    public $affectedrows;

    /**
     * Last insert id of the query
     * @var array
     */
    public $insertid;
    
    /**
     * The error number from the query, if any
     * @var array
     */
    public $errno;
    
    /**
     * Run init operations
     *
     * @param array $dbMap map of database connection information
     * @param mixed $conn the connection to run the query on
     * @param string $type type of connection to make to database
     * 
     * @return mixed  
     */
    public function __construct($dbMap, $connKey = "", $type = ""){
        // Map of availavle db connection info    
        $this->dbMap = $dbMap;
        
        //set db type
        if (!empty($type)) {
            $this->type = $type;
        } else {
            $this->type = "mysqli";
        }
        
        // check if connection object passed
        if ($connKey instanceof mysqli) {
            $this->dbconn = $connKey;
        } else if (!empty($connKey)) {
            // get instance of database connection cache with specified connection key name
            $dbCache = BK_Db_Cache::getInstance($this->dbMap, $connKey);
            
            // set local reference of database connection
            $this->dbconn = $dbCache->getConnection();
        } else {
            // get instance of database connection cache, with default connecetion 
            $dbCache = BK_Db_Cache::getInstance($this->dbMap);
            
            // set local reference of database connection
            $this->dbconn = $dbCache->getConnection();
        }
    }   
    
    /**
     * Turn on implicit transactions by setting autocommit to false on the connection object.
     * 
     * @return void  
     */
    public function beginTran(){
        $this->dbconn->autocommit(false);
    }
    
    /**
     * Rollback current transaction and turn on auto commit for the connection
     * 
     * @return void  
     */
    public function rollbackTran(){
        $this->dbconn->rollback();
        $this->dbconn->autocommit(true);
    }
    
    /**
     * Commit current transation and turn on auto commit for the connection
     * 
     * @return void  
     */
    public function commitTran(){
        $this->dbconn->commit();
        $this->dbconn->autocommit(true);
    }
    
    //===========================================
    // clear result data to run another query
    //===========================================
    public function clear(){
        $this->msg = "";
        $this->rows = array();
        $this->rowcount = "";
        $this->affectedrows = "";
        $this->errno = "";
    }
    
    //===========================================
    // close db connection
    //===========================================
    public function close(){
        if($this->dbconn instanceof mysqli){
            $this->dbconn->close();
        }
    }
    
    //===========================================
    // run query
    //===========================================
    public function runQuery($query, $params = ""){
        //clear previousinfo
        $this->clear();

        // debug info
        // echo $query."<br />";
        
        //determine db to run base on intilized type
        if ($this->type == "mysqli") {
            return $this->runMySqli($query, $params);
        } else if ($this->type == "mysql") {
            return $this->runMySql($query, $params);
        }
        else {
            return $this->runMySqli($query, $param);
        }
        
    }//end function runQuery
    
    
    //===========================================
    // run query using MySQLi
    //===========================================
    public function runMySqli($query, $params, $qtype = "statement"){
        //run as perpared statement
        if ($qtype == "statement") {
            //prepare statement
            $stmt = $this->dbconn->prepare($query);
            
            // check if statment was prepared ok
            if ($stmt) { 
                //bind parameters
                if(is_array($params) && !empty($params)){
                    $funcparams = array();
                    $funcparams["statement"] =& $stmt;
                    
                    $paramtypes = '';
                    foreach($params as $key => $value){
                        //build parameter types
                        if (is_int($value)) {
                            $paramtypes .= "i";
                        } else if (is_string($value)) {
                            $paramtypes .= "s";
                        } else if (is_float($value)) {
                            $paramtypes .= "d";
                        } else {
                            $paramtypes .= "b";
                        }
                        $funcparams["paramtypes"] =& $paramtypes; //parameter types
                        $funcparams[$key] =& $params[$key];
                    }
                    
                    // Bind parameters, we call this functon because we don't know how many parameters come in each time
                    // so wee need to dynamically call the bind with different number of param arguments 
                    call_user_func_array('mysqli_stmt_bind_param', $funcparams);
                }

                // execute statement
                mysqli_stmt_execute($stmt);
                
                //store results
                if($this->storeresult == true){
                    mysqli_stmt_store_result($stmt);
                }
                
                //bind result
                $funcbindparams["statement"] =& $stmt;
                $metadata = mysqli_stmt_result_metadata($stmt); //store result metadata
                if($metadata !== false){
                    $finfo = mysqli_fetch_fields($metadata);
                    $out = array();
                    foreach ($finfo as $val){
                        if (!empty($val->table) && $this->fqnames) {
                            $funcbindparams[$val->table.".".$val->name] = &$out[$val->table.".".$val->name];
                        } else { 
                            $funcbindparams[$val->name] = &$out[$val->name];
                        }
                    }
                    call_user_func_array('mysqli_stmt_bind_result', $funcbindparams);
                }
                
                //fetch data
                $count = 0;
                while (mysqli_stmt_fetch($stmt)){
                    foreach($out as $key => $value){
                        $this->rows[$count][$key] = $value;
                    }
                    $count++;
                }
                
                //fetch metadata
                $this->rowcount = $count; 
                $this->affectedrows = mysqli_affected_rows($this->dbconn); 
                $this->insertid = mysqli_insert_id($this->dbconn);
                
                
                //close statement
                mysqli_stmt_close($stmt);
                
                //return success
                return true;
            } else {
                // record metadata
                $this->errno = mysqli_errno($this->dbconn);

                //return failure
                return false;
            }// end if check statment onbject
        }
        //run as query
        else{
            //return success
            return true;
        }
    }//end fuction runMySqli
    
    //===========================================
    // run query using MySQL
    //===========================================
    public function runMySql($query, $params, $qtype = "statement"){
        // replace parameters
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                $query = substr_replace($query, "'".mysql_real_escape_string($value)."'", strpos($query, "?"), 1);
            }
        }
        
        // run query
        $result = mysql_query($query, $this->dbconn);
        
        if (!$result) {
            //return success
            return false;
        } else {
            //fetch metadata
            $this->rowcount = @mysql_num_rows($result); 
            $this->affectedrows = @mysql_affected_rows($this->dbconn); 
            $this->insertid = @mysql_insert_id($this->dbconn);
            
            //fetch data
            for ($i=0;$i<$this->rowcount;$i++){
                for ($j=0;$j<mysql_num_fields($result);$j++) { 
                    if ($this->fqnames) {
                        $this->rows[$i][mysql_field_table($result, $j).".".mysql_field_name($result,$j)] = mysql_result($result,$i,mysql_field_name($result,$j));
                    } else {
                        $this->rows[$i][mysql_field_name($result,$j)] = mysql_result($result,$i,mysql_field_name($result,$j));
                    }
                }
            }//end for  
            
            //close statement
            @mysql_free_result($result);
        }
        
        
        //return success
        return true;
        
    }//end fuction runMySql

}//end class