define(['backbone/backbone'], function(Backbone){
	
	/**
	 * Stream
	 *
	 * Activity data for an event
	 */
	var Stream = Backbone.Collection.extend({
	    source: '',
	    initialize : function(models, options) {
	 
	        // set the url
	        if(options.url) {
	            this.url = options.url;
	        }



	        // set collection base model
	        if(options.model) {
	            this.model = options.model;
	        }

	        
	        // update collection every 10 seconds
	        setInterval((function(self) {
	            return function() {
	                self.fetch({
	                    add : true
	                });
	            };
	        })(this), 100000);
	    },
	    parse : function(response) {
	        // Check for "data" property, means we are getting it back directly from server
	        if(response.data) {
	            return response.data;
	        } else {
	            return response;
	        }
	    }
	});

	return Stream;
});