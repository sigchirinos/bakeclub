<?php 
/**
 * Handles access control for different routes
 * @package Bake
 */
namespace Bake;

class Acl {

    /**
     * Allows only the passed roles to continue, denies everyone else and throws an exception
     * @param array $roles roles to check session for
     * @return function
     */
    public function allow($roles) 
    { 
      // Slim middleware needs a "callable" to execuate so we return an anonymous function
      return function (\Slim\Route $route) use ($roles) { 
        // Get the roles associated with the user
        $current_roles = (isset($_SESSION['roles']) && is_array($_SESSION['roles']) ? $_SESSION['roles'] : array());

        // Checks the passed in roles against the ones associated with the current user
        $result = array_intersect($roles, $current_roles);

        // We didn't find a role that was allowable
        if (empty($result)) {
          throw new \Bake\AppException("Forbidden", 403);
        }
      };
    }

    /**
     * Deny the request to the roles passed in, allow everyone else and throws an exception
     * @param array $roles roles to check session for
     * @return function
     */
    public function deny($roles) 
    { 
      // Slim middleware needs a "callable" to execuate so we return an anonymous function
      return function (\Slim\Route $route) use ($roles) {
        // Get the roles associated with the user
        $current_roles = ((isset($_SESSION['roles']) && is_array($_SESSION['roles'])) ? $_SESSION['roles'] : array());

        // Checks the passed in roles against the ones associated with the current user
        $result = array_intersect($roles, $current_roles);

        // We found one of the roles that need to be denied
        if (!empty($result)) {
          throw new \Bake\AppException("Forbidden", 403);
        }
      };
    }
}