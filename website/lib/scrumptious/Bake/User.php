<?php 
/**
 * User management library
 * @package Bake
 * @author Sigfrido Chirinos
 */
namespace Bake;

class User {
    
    /**
     * The database abstraction object to query a database
     * @var mixed
     */
    public $orm;

    /**
     * Object settings
     * source - [db|ldap] The authentication source type 
     * @var array
     */
    public $settings = array();

    /**
     * Constructor
     * @param string $settings object settings
     */
    public function __construct($settings = array()) 
    {
        // Overwrite default settings
        $this->settings = array_merge([
            'orm' => \ORM::for_table('users')
            ], $settings
        );

        // connection to default database
        $this->orm = $this->settings['orm'];
    }

    /**
     * Create a new user
     * @param array $options user information 
     * @return boolean
     */
    public function create($options) 
    {
        // Merge default options with passed options
        $options = array_merge([
            'username' => '',
            'password' => '',
            'fname' => '',
            'lname' => '',
            'email' => '',
            'about' => ''
            ], $options);

        // Filter and validate user creation params

        // Start transaction handling
        \ORM::get_db()->beginTransaction();

        // Initialize auth object
        $auth = new Auth();

        try {
            // Create authentication record, returns insert id
            $id = $auth->createAuthRecord($options['username'], $options['password'])['data']['id'];
        } catch (\Exception $e) {
            // Roll back a transaction
            \ORM::get_db()->rollBack();

            // Rethrow the exception, to be handled by  global handlers
            throw $e;
        }

        // Unset the password, because we don't want to return that info back to the user
        unset($options['password']);

        // Create user profile information
        $this->orm->create();
        $this->orm->id = $id;
        $this->orm->fname = $options['fname'];
        $this->orm->lname = $options['lname'];
        $this->orm->email = $options['email'];
        $this->orm->about = $options['about'];

        try {
            // Try to save back to db
            $this->orm->save();

            // Commit a transaction
            \ORM::get_db()->commit();
        } catch (\Exception $e) { 
            // Roll back a transaction
            \ORM::get_db()->rollBack(); 

            // Rethrow the exception, to be handled by  global handlers
            throw $e;
        }
        
        // Return the user information entered
        return array("data" => $options, "meta" =>  new \stdClass);
    }
     
}