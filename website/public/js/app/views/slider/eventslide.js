/**
    Represents an event slide in a slider.
    @module app/views/slider/eventslide
    @extends app/views/base
 */
define([
    'app/views/base',
    'text!app/tpl/slider/event_slide.html',
    'app/views/dash/eventinfo'
    ],
function(BaseView, Tpl, EventInfoView){
    /**
     * @constructor
     * @alias module:app/views/slider/eventslide
     */
    var exports = BaseView.extend({
        /**
            Reference to the slide container
            @type {app/views/slider/slider}
            @default
         */
        parentSlider : {},

        /**
            HTML tag to use for the view's root element.
            @type {String}
            @default
         */
        tagName : 'li',

        /**
            HTML class for the view's root element.
            @type {String}
            @default
         */
        className : 'slide-item',

        /**
         * Specifies the event handlers and which functions the wire up to.
         * @type {Object}
         */
         events: {
            "click a.details"       : "showDetail",
            "dblclick "             : "showDetail"
         },

        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);

            // set the background image
            this.$el.css('background-image', 'url("' + this.model.get('path') + '")');
        },

        /**
         * Show detailed item information.
         * @param {DOM Event} event The object for the fired event
         */
         showDetail: function(event){
            // capture clicks
            event.preventDefault();

            // Create new Event info view, passing this slide's model
            var eventInfo = new EventInfoView({model: this.model});

            // Add event info view to the dashboard
            this.router.app.views['.main'].addView(eventInfo, true);
         }

    });

    return exports;
});