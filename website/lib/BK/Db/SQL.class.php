<?php 
/**
 * SQL Abstraction object, helps to make basic CRUD (create, read, update, delete) queries 
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Db_SQL {
    /*
    * Property: $query
    * Type: Object
    * Database query abstraction object
    */
    public $query;
    
    /*
    * Property: $tablename
    * Type: String
    * The table name to work on
    */
    protected $tablename;
    
    /*
    * Property: $fields
    * Type: Array
    * Fields to retrieve
    */
    public $fields = array("*");
    
    /*
    * Property: $limit
    * Type: String
    * Limit clause
    */
    protected $limit = "";
    
    /*
    * Property: $orderby
    * Type: String
    * Order by clause
    */
    protected $orderby = "";
    
    /*
    * Property: $msg
    * Type: String
    * Status messages for object
    */
    public $msg;
    
    /*
    * Property: $storeresult
    * Type: Boolean
    * Whether to enabled storing result on client for mysqli object
    */
    public $storeresult = false;
        
    /*
    * Property: $rows
    * Type: Array
    * Stores information retrieved from database (always an array of rows)
    */
    public $rows = array();
    
    //----------------
    // [constructor]
    //----------------
    public function __construct($table, $queryObj){
        // set query object
        $this->query = $queryObj;  
        
        // set base table name
        $this->tablename = $table;
    }   
    
    //===========================================
    // create 
    //===========================================
    public function create($info){
        //build fields to insert
        foreach($info as $key => $value){
            $columns[$key] = $key;
            
            // set value , do not add parameter is value is sql keyword "NOW()"
            if ($value === 'NOW()'){
                $values[$key] = "NOW()";
            } else {
                $values[$key] = "?";
                $params[$key] = $value;
            }
        }

        // Init variables
        $sql_fields = '';
        $sql_values = '';

        $sql_fields .= implode(",", $columns);
        $sql_values .= implode(",", $values);
        
        // run query
        $query = "INSERT INTO $this->tablename ($sql_fields) VALUES ($sql_values)"; 
        $this->query->runQuery($query, $params);
        
        // check result
        if ($this->query->affectedrows > 0) { 
            $this->msg =  "Created successfully.";
            $status = true;
        } else {
            $this->msg =  "Error trying to create.";
            $status = false;
        }
        
        // return   
        return $status;
    }//end create
    
    
    //===========================================
    // delete
    //===========================================
    public function delete($inparams){  
        // init variables
        $sql_where = "";
        $sql_whereArr = array();
        
        // process parameters
        if (is_array($inparams)) {
            // build where clause
            foreach ($inparams as $key => $value) {
                $sql_whereArr[] = $key." = ?";
                $params[$key] = $value; // add paramter
            }
            
            // join array into string 
            if (count($sql_whereArr) > 0) {
                $sql_where = "WHERE ".implode(" AND ", $sql_whereArr);  
            }
        } 
        // disallow empty inparams
        else if (empty($inparams)) {
            $this->msg =  "Error trying to delete.";
            return false;
        } 
        // default to id 
        else {
            $sql_where = "WHERE id = ?";
            $params['id'] = $inparams; // add paramter
        }
        
        // run query
        $query = "DELETE FROM $this->tablename $sql_where $this->limit";
        $this->query->runQuery($query, $params);

        // check result
        if($this->query->affectedrows > 0){
            $this->msg =  "Deleted successfully.";
            $status = true;
        }else{
            $this->msg =  "Error trying to delete.";
            $status = false;
        }
        
        // return
        return $status;
    }//end delete
    
    //===========================================
    // read 
    //===========================================
    public function read($inparams){
        // init variables
        $sql_where = "";
        $sql_whereArr = array();
        
        // process parameters
        if (is_array($inparams)) {
            // build where clause
            foreach ($inparams as $key => $value) {
                $sql_whereArr[] = $key." = ?";
                $params[$key] = $value; // add paramter
            }
            
            // join array into string 
            if (count($sql_whereArr) > 0) {
                $sql_where = "WHERE ".implode(" AND ", $sql_whereArr);  
            }
        } 
        // disallow empty inparams
        else if (empty($inparams)) {
            $this->msg =  "Error trying to read.";
            return false;
        } 
        // default to id 
        else {
            $sql_where = "WHERE id = ?";
            $params['id'] = $inparams; // add paramter
        }
        
        // run query
        $query = "SELECT ".implode(",", $this->fields)." FROM $this->tablename $sql_where $this->orderby $this->limit";
        $this->query->runQuery($query, $params);
        
        // check result
        if($this->query->rowcount > 0){
            if ($this->query->rowcount == 1){
                $this->data = $this->query->rows[0];     // read in single row into array
                $this->rows = $this->query->rows;
            } else {
                $this->rows = $this->query->rows;        // read in multiple rows in array
            }
            $this->msg =  "Read successfully.";
            $status = true;
        }else{
            $this->msg =  "Error trying to read.";
            $status = false;
        }
         
        // return
        return $status;
    }
    
    //===========================================
    // read whole table
    //===========================================
    public function readAll(){  
        //get userid
        $query = "SELECT ".implode(",", $this->fields)." FROM $this->tablename $this->orderby $this->limit";
        $this->query->runQuery($query);
        
        //check result
        if($this->query->rowcount > 0){
            $this->rows = $this->query->rows;     // read in single row into array
            $this->msg =  "Read successfully.";
            $status = true;
        }else{
            $this->msg =  "Error trying to read.";
            $status = false;
        }
        
        // return   
        return $status;
    }
    
    //===========================================
    // read custom query
    //===========================================
    public function readCustom($query, $params = array()){  

        // Add limit clause if available
        if (!empty($this->limit)) {
            $query = $query.$this->limit;
        }

        // run query
        $this->query->runQuery($query, $params);
        
        //check result
        if($this->query->rowcount > 0){
            if ($this->query->rowcount == 1){
                $this->data = $this->query->rows[0];     // read in single row into array
                $this->rows = $this->query->rows;
            } else {
                $this->data = $this->query->rows;        // read in multiple rows in array
                $this->rows = $this->data;              // read in multiple rows in array
            }
            $this->msg =  "Info retrieved successfully.";
            $status = true;
        } else {
            $this->msg =  "Error trying to retrieve info.";
            $status = false;
        }
        
        //clear result info and return  
        return $status;
    }
    
    //===========================================
    // update 
    //===========================================
    public function update($id, $info){ 
        // init variables
        $sql_where = "";
        $sql_whereArr = array();
            
        // build fields to SET update
        foreach($info as $key => $value){
            // set value , do not add parameter is value is sql keyword "NOW()"
            if ($value === 'NOW()'){
                $sql_fields[$key] = $key . " = NOW()";
                unset($info[$key]); // unset corresponding paramter in original array
            } else {
                $sql_fields[$key] = $key . " = ?";
            }
        }
        $sql_set = "";
        $sql_set .= implode(",", $sql_fields);
       
        
        // process Where clause fields
        if (is_array($id)) {
            // build where clause
            foreach ($id as $key => $value) {
                $sql_whereArr[] = $key." = ?";
                $info[] = $value; // add paramter
            }
            
            // join array into string 
            if (count($sql_whereArr) > 0) {
                $sql_where = "WHERE ".implode(" AND ", $sql_whereArr);  
            }
        } 
        // disallow empty id
        else if (empty($id)) {
            $this->msg =  "Error trying to update.";
            return false;
        } 
        // default to id 
        else {
            $sql_where = "WHERE id = ?";
            $info['id'] = $id; //add id to info array
        }
        
        // run query
        $query = "UPDATE $this->tablename SET ".$sql_set." $sql_where $this->limit";
        $this->query->runQuery($query, $info);
       
        //check result
        if($this->query->affectedrows > 0){
            $this->msg =  "Updated successfully.";
            $status = true;
        } else if ($this->query->affectedrows == 0){
            $this->msg =  "Nothing to update.";
            $status = false;
        } else {
            $this->msg =  "Error trying to update.";
            $status = false;
        }
        
        // return   
        return $status;
    }
    
    //===========================================
    // update custom query
    //===========================================
    public function updateCustom($query, $params = array()){    
        // run query
        $this->query->runQuery($query, $params);
        
        //check result
        if ($this->query->affectedrows > 0) {
            $status = true;
        } else if ($this->query->affectedrows == 0){
            $status = false;
        } else {
            $status = false;
        }
        
        //clear result info and return  
        return $status;
    }// end function updateCustom
    
    //===========================================
    // set limit clause
    //===========================================
    public function limit($limit){
        // set limit
        $this->limit = $limit;
        
        // enables chaining
        return $this;
    }

    /**
     * Creates and sets the limit clause based on page and record count
     * 
     * @param int $page The page to start with
     * @param int $recordCount Number of records on a page
     *
     * @return __CLASS__ used for chaining
     */
    public function setPage($page, $recordCount){

        // Calculate offset for limit clause
        if (is_numeric($page) && $page > 0) {
            if (is_numeric($recordCount) && $recordCount > 0) {
                $offset = ($page - 1) * $recordCount;
            } else {
                // Default to 30 records for limit
                $recordCount = 30;
                $offset = ($page - 1) * $recordCount;
            }
            
            // set limit clause
            $this->limit = " LIMIT ".$offset.", ".$recordCount;
        }

        
        // enables chaining
        return $this;
    }
    
    //===========================================
    // setOrderBy
    //===========================================
    public function orderBy($order){
        // set ordering
        $this->orderby = $order;
        
        // enables chaining
        return $this;
    }
    
    //===========================================
    // set storing result on client flag
    //===========================================
    public function storeResult($flag){ 
        $this->query->storeresult = flag;
        
        // enables chaining
        return $this;
    }
        
}//end class