<?php 
// This bootstrap file initialized and configures a bunch of things that we usually need for each request.
// Session handling, db connection setup and global error handler.
// You can copy this file and modify it to create different bootstraps for different api endpoints.

// Import libs
require '../lib/vendor/autoload.php';

// Prepare db connection pool 
// (doesn't make a connection until you create the ORM objects later)
\Bake\Config::prepareDB();

// Init session handling 
$apikey = new \Bake\Key();

// Init slim framework
$app = new \Slim\Slim([
	// Setting to false allows us to use our custom error handler
	'debug' => false,
	// Inject apikey into app
	'apikey' => $apikey,
	'version' => "0.1",
	'swaggerversion' => "1.2"
]);

// Add json content type header to every request
$app->hook('slim.after.router', function () use ($app) {
    $app->response()['Content-Type'] = 'application/json';
});

// Production mode options
$app->configureMode('production', function () use ($app) {

	// Error handler
	$app->error(function ($e) use ($app) {
		// Log error
		$log = $app->getLog()->error($e);

		// Only display Application exceptions to user
		if ($e instanceof \Bake\AppException) {
			// Set http status code base on exception code
			$app->response()->status($e->getCode());

			// Return error message
			echo json_encode(['data' => new stdClass(), 'meta' => ['code' => $e->getCode(), 'msg' => $e->getMessage()]]);	
		} else {
			// We don't want to expose deeper errors to just return generic server error
			$app->halt(500, json_encode(['data' => new stdClass(), 'meta' => ['code' => 500, 'msg' => 'Internal server error']]));
		}
	});
});

// Development mode options
$app->configureMode('development', function () use ($app) {

	// Error handler
	$app->error(function ($e) use ($app) {
		// Log error
		$log = $app->getLog()->error($e);

		// Set http status code base on exception code
		$app->response()->status($e->getCode());

		// Return error message
		echo json_encode(['data' => new stdClass(), 'meta' => ['code' => $e->getCode(), 'msg' => $e->getMessage()]]);
	});
});


