/** 
    Dashboard widget provide a workspace for all app content and manages tabbing for content
    @module app/views/dash/dash
    @extends app/views/base
 */
define([
    'app/views/base',
    'text!app/tpl/dash/container.html',
    'text!app/tpl/dash/tab.html',
    'text!app/tpl/dash/tabpane.html',
    ], 
function(BaseView, Tpl, TplTab, TplTabPane){
    /** 
     * @constructor
     * @alias module:app/views/dash/dash
     */
    var exports = BaseView.extend({
        /** 
            A hash index of all the views loaded into the dash
            the hash is generated from the views model type and id from the server
            @type {Array}
         */
        registry : [],

        /** 
            Hash of events to wire to functions
            @type {Object}
         */
        events : {
            "click  .nav .closepane"    : "closePane"
        },

        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // Compile template
            this.template = _.template(Tpl);
        },

        /**
         * Create tab markup
         * @param {app/views/dash/} view The view to add a tab for
         * @param {String} clientIdent The unique identifier for the view to use in the tab
         * @returns {String} The HTML for the tab
         */
        createTab : function(view, clientIdent) {
            // Compile tab template
            var elemTab = _.template(TplTab);

            // See if we can get a title from the model, if not default to what is on view
            var title = (view.model && view.model.get("title")) ? view.model.get("title") : view.options.title;

            // Render template into html string
            var tplHtml = elemTab({'client_ident': "tab-"+clientIdent, 'title': title});

            // Return tab element
            return tplHtml;
        },

        /**
         * Create tab pane markup
         * @param {app/views/dash/} view The view to add to the dashboard
         * @param {String} clientIdent The unique identifier for the tab pane
         * @returns {String} The HTML for a tab pane
         */
        createTabPane : function(view, clientIdent) {
            // Compile template for tab container
            var el = _.template(TplTabPane);

            // Render template into html string
            var tplHtml = el({'client_ident': "tab-"+clientIdent});

            // Return tab element
            return $(tplHtml, "tab-"+clientIdent).append(view.render().el);
        },

        /**
         * Add a new view to the dashboard
         * @param {app/views/dash/} view The view to add to the dashboard
         * @param {Boolean} show Flag to determine whether to display the added view right away or not
         * @returns undefined
         */
        addView : function(view, show) {

            // Generate the view's unique hash
            var viewHash = this.generateViewHash(view);

            // See if we already added this view to the dashboard
            if (_.indexOf(this.registry, viewHash) !== -1) {

                // Display the view's content
                $("a[href='#tab-"+ viewHash + "']", this.$el).tab('show');

                // do nothing else
                return;
            }

            // This is a new view so we need to add it to the dashboard

            // Create new tab
            var newTab = $(this.createTab(view, viewHash));

            // If there is more than one tab show the tab list
            if ($('.nav li', this.$el).length > 1) {
                
            } else {
                // Otherwise hide the only tab, so that the user can't close it and have an empty page
                this.$('ul.nav').hide();
            }

            // Add tab
            $('[data-dashboard-nav]', this.$el).append(newTab);

            // Add the view to content area
            var newPane = this.createTabPane(view, viewHash);
            $('[data-dashboard-content]', this.$el).append(newPane);

            // Register view, in order to prevent duplicate tabs
            this.registry.push(this.generateViewHash(view));
            
            // Do we display the new item now?
            if (show) {
                // We need to make tab hidden first before we show it
                // so that we can fade it in properly using the bootstrap fade class
                // If we just added the fade class first the bootstrap code would automatically remove it
                $("#tab-"+ viewHash, self.$el).addClass("hidden");
                $("[data-dashboard-nav] a[href='#tab-"+ viewHash + "']", this.$el).tab('show');
                $("#tab-"+ viewHash, self.$el).addClass("fade in").removeClass('hidden');



            } else {
                $("#tab-"+ viewHash, self.$el).addClass("fade");
            }
        },

        /**
         * Remove story and tab from display
         */
        closePane : function(e) {
            // capture bubble ups
            e.preventDefault();

            // Get identity of the story to remove
            var ident = $(event.target).parent().attr("href");

            // Remove tab
            $("a[href='"+ ident + "']", this.$el).parent().remove();

            // Remove tab pane
            $(ident, this.$el).remove();

            // If only
            if ($('.nav li', this.$el).length <= 1) {
                
            } else {
                // Show last tab
                $('[data-dashboard-nav] a', this.$el).last().tab('show');  
            }
            
        },

        /**
         * Generate a hash based on the views info. This is used to identify the view in the dashboard registry so we don't load multiple tabs
         * of the same thing
         * @param {app/views/dash/} view The view to generate the hash for
         * @returns {String} 
         */
        generateViewHash : function(view) {
            var viewHash;

            if (view.model && view.model.type) {
                viewHash = view.model.type + view.model.get('id');
            } else {
                viewHash = view.cid;
            }

            return viewHash;
        }
    });

    return exports;
});