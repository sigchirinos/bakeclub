<?php 
/**
 * HTTP response object
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_HTTP_Response {
    
    /**
     * Response body
     * @var string
     */
    public $body;
    
    /**
     * HTTP status code
     * @var string
     */
    public $statusCode;
    
    /**
     * HTTP status code text
     * @var string
     */
    public $status;
    
    /**
     * Array of all HTTP statuses
     * @var array
     */
    private $statusText = array(
            200 => 'OK',
            400 => "Bad Request",
            401 => "Unauthorized",
            500 => "Internal Server Error"
    );
    
    /**
     * Set the status code and status text
     * 
     * @param int $statusCode HTTP status code
     * 
     * @return void  
     */
    public function setStatus($statusCode) {
    
        // set the status code
        $this->statusCode = $statusCode;
        
        // Set match status text
        $this->status = $this->statusText[$statusCode];

    }
}