/** 
    Display's all the information for an event
    @module app/views/dash/eventinfo
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/views/streamer/streamer',
    'app/collections/stream',
    'app/models/note',
    'app/models/tweet',
    'app/models/comment',
    'app/models/media',
    'text!app/tpl/dash/eventinfo.html'
    ],
function(BaseView, StreamerView, StreamColl, Note, Tweet, Comment, Media, Tpl){
    /**
     * @constructor
     * @alias module:app/views/dash/eventinfo
     */
    var exports = BaseView.extend({

        /**
            HTML tag to use for the view's root element.
            @type {String}
            @default
         */
        tagName: 'div',

        /**
            HTML class for the view's root element.
            @type {String}
            @default
         */
        className: 'dash-item',

        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);

            // Create new streamer to show info related to the event
            this.streamer = new StreamerView({
                template: 'simple',
                collections : {
                    "note" : new StreamColl([], {
                        url : '/api/notes/event/'+this.model.get('id'),
                        model : Note
                    })
                }
            });
        },

        postRender: function () {
            this.$el.append(this.streamer.render().el);
        }

    });

    return exports;
});