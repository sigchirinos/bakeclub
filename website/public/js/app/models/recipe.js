define(['backbone/backbone'], function(Backbone){
	
	/**
	 * Recipe
	 */
	var Recipe = Backbone.Model.extend({
	    urlRoot : '/api/recipe',

	    defaults : {
	        "title" : "",
	        "summary" : ""
	    },

	    parse : function(response) {
	        // Check for "data" property, means we are getting it back directly from server
	        if(response.data) {
	            return response.data;
	        } else {
	            return response;
	        }
	    }
	});

	return Recipe;
});