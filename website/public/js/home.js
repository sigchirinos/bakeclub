/**
 * @file
 * This is the applications bootstrap file. It sets configuration options then kicks off
 * the application.
 */
// requirejs configuration
requirejs.config({
    baseUrl: '/public/js/lib',
    paths: {
        app: '../app',
        backbone: 'backbone',
        bootstrap: 'bootstrap',
        cubiq: 'cubiq'
    },
    // Shim config for js libraries that don't use "define"
    shim: {
        'backbone/underscore': {
            exports: "_"
        },
        'backbone/backbone': {
            deps: ['jquery', 'backbone/underscore'],
            exports: 'Backbone'
        },
        'cubiq/iscroll': {
            exports: "iScroll"
        },
        'bootstrap/bootstrap': {
            deps: ['jquery']
        }
    }
    
});

// Start the main app logic.
requirejs([
    'domReady!',
    'app/routers/main',
    'app/views/base',
    'app/views/dash/dash',
    'app/views/header',
    'app/views/footer',
    'bootstrap/bootstrap'
],
function (doc, Router, BaseView, DashView, HeaderView, FooterView) {

    // To prevent creating too many session ids, since we fire off lots of HTTP request in pararllel
    // we kick off app with a single call to api to get the apikey for this session
    $.ajax('/api/auth/key')

    // Execute on success
    .done(function () {
        // Initiate the application router
        var app_router = new Router({
            appView: new BaseView({
              el    : $('.workspace'),
              views : {'header': new HeaderView(), '.main': new DashView()}
            })
        });

        // Start Backbone history a neccesary step for bookmarkable URL's
        Backbone.history.start({pushState: true});
    })

    // Execute on failure
    .fail(function () {
        // Ask user to reload app. TODO: provide button to reload
        alert('App initialization failed, try reloading the page');
    });
});