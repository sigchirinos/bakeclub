/**
    The base view from which all views in the app derive.
    @module app/views/base
    @extends Backbone.View
 */
define([
    'backbone/backbone'
    ],
function(Backbone, Dispatcher){
    /**
     * Automatically initializes child views when instantiated by calling <b>initChildViews</b>
     * @constructor
     * @alias module:app/views/base
     * @param {Object} options To specify child views. Pass a views object under the "views" key to the options object.
     * The views object should be key value pairs where the key is a css selector (ex: .main) and the value is instance of a Backbone view.
     * <br /> `Ex: var myView = new BaseView(views : {'.main':new BakeView()});`
     */
    var exports = Backbone.View.extend({
        /**
            Holds child views
            @type {app/views/bake}
         */
        views : {},

        /**
            The template for the view
            @type {Object}
         */
        template : {},

        /**
            Holds a reference to the application router
            @type app/routers/main
         */
        router : {},

        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // need to init local copy of child view
            // object to maintain proper protype chain
            this.views = {};
            
            // Create child views, for rendering later
            this.initChildViews();
        },
        /**
         * Iterates through child views passed into constructor options and attaches them to the object.
         * Keys are based on the selector
         */
        initChildViews : function() {
            if(this.options.views) {
                // Iterate through child view definitions and initialize
                for(var selector in this.options.views) {
                    // Assign class to variable, just to make typing shorter
                    var myObj = this.options.views[selector];

                    // Check if an instatiated object was passed
                    if(_.isFunction(myObj)) {
                        // Instantiate new instance of view
                        this.views[selector] = new myObj();
                    } else {
                        // An already instantiated object was passed, add it to child views array
                        this.views[selector] = myObj;
                    }

                    // Inject reference to the parent view's router
                    this.views[selector].router = this.router;
                }
            }
        },
        /**
         * Called before the main rendering logic takes place
         */
        preRender : function() {

        },
        /**
         * Called after the main rendering logic takes place
         */
        postRender : function() {

        },
        /**
         * Attach the view's HTML template to the DOM and do the same for child views.
         * Rendering happens in top down fashion with the base view being added to the DOM first then each subsequent child.
         * @returns this for chaining
         */
        render : function() {
            // Pre render actions
            this.preRender();

            // Check to see if template was compiled by seeing if it is a function
            if (_.isFunction(this.template)) {
                // Is a model set on the view
                if(this.model) {
                    this.$el.html(this.template(this.model.toJSON()));
                } else {
                    this.$el.html(this.template());
                }
            }
            
            // Post render actions take place before we render each child
            this.postRender();

            // Render child views
            this.renderChildren();

            // For chaining
            return this;
        },
        
        /**
         * Initialize child views
         *
         */
        renderChildren : function() {
            // Check if child views were set and render child views
            if(this.views) {
                for(var selector in this.views) {
                    // Attach view to element found by selector
                    this.views[selector].setElement($(selector, this.el));
                    
                    // Render to DOM
                    this.views[selector].render();
                }
            }
        }
    });
   
    return exports;

});