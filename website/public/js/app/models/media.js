define(['backbone/backbone'], function(Backbone){
	
	/**
	 * Media
	 */
	var Media = Backbone.Model.extend({
	    parse : function(response) {
	        // Check for "data" property, means we are getting it back directly from server
	        if(response.data) {
	            var data = response.data;
	        } else {
	            var data = response;
	        }

	        // Create client unique identifier
	        data.client_ident = "media" + data.id;

	        return data;
	    }
	});

	return Media;
});