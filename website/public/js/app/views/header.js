/**
    Header view
    @module app/views/header
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/views/login',
    'text!app/tpl/header.html'
    ],
function(BaseView, LoginView, Tpl){
    /**
     * @constructor
     */
    var exports = BaseView.extend({
        /**
         * Compile template
         */
        template : _.template(Tpl),
 
        /**
         * Events
         */
        events : {
            "click nav ul li.section a"     : "loadSection"
        },

        /**
         * Init stuff
         */
        initialize : function() {
            // Call parent in order to create views array
            BaseView.prototype.initialize.call(this);

            // Add login as child view
            this.views['.login'] = new LoginView();
        },
        /**
         * Handles clicks for loading different sections of the site
         * @param {Event} e The event object for the fired event
         */
        loadSection : function(e) {
            // Don't execute page change
            e.preventDefault(); e.stopPropogation();

            var elem;

            // we need to be on the Anchor link
            if(event.target.tagName !== "A") {
                if(event.target.parentNode) {
                    elem = event.target.parentNode;
                }
            } else {
                elem = event.target;
            }

            // turn off all other buttons
            if(elem && elem.parentNode && elem.parentNode.parentNode) {
                $('.on', elem.parentNode.parentNode).removeClass("on");

                $(elem.parentNode).addClass("on");
            }

            // Get section we clicked on from label
            var section = $(elem).find('span.sec-name').text();

            // Set section name display in header
            $('span.section', this.$el).html(section);

            // TODO: Get href of anchor and navigate through router to load the section
            // Need to pass the main app router through to child views
            // var href = event.target.parentNode.href;

            // get last height
            var cur_height = $('#wrap-app').height();
            var last_height = $('#wrap-app').attr('last-height');

            // Load proper section
            if(section == 'About') {

                // Set last height to current height if last_height is not set
                if(!last_height) {
                    $('#wrap-app').attr('last-height', cur_height);
                }

                $('footer .resources').hide();

                // Rollup screen
                $('footer .about').show(0, function() {
                    $('#wrap-app').animate({
                        height : '115px'
                    });
                });
            } else if(section == 'Resources') {

                // Set last height to current height if last_height is not set
                if(!last_height) {
                    $('#wrap-app').attr('last-height', cur_height);
                }

                $('footer .about').hide();

                // Rollup screen
                $('footer .resources').show(0, function() {
                    $('#wrap-app').animate({
                        height : '115px'
                    });
                });
            } else {

                // Scroll main app area into view
                if(last_height) {
                    $('#wrap-app').animate({
                        height : last_height
                    }, function() {
                        $('footer .about').hide();
                        $('footer .resources').hide();
                    });
                }
            }

        }
    });

    return exports;
});