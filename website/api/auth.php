<?php 
// Bootstrap application
require 'bootstrap.php';

// We use this object later as route middleware
// to implement roles based access control
$acl = new \Bake\Acl();

// Create swagger doc object
$apiDoc = new \Slagger\APIDoc('/auth/api-doc/auth', ['resourcePath' => '/auth']);

// Inject swagger documentation generation middleware
$app->add($apiDoc);

//---------------------------------
// Echo the api key name and value
//---------------------------------
$app->get('/auth/key', $apiDoc->routeDoc('key', [
	
	// Swagger doc
	"summary" => "Generates and apikey"

]), function () use ($app) {

    // Output api key id
	echo json_encode([$app->config('apikey')->name => $app->config('apikey')->val]);

})->name('key');

//---------------------------------
// Authenticate user
//---------------------------------
$app->post('/auth/login', $apiDoc->routeDoc('login', [

	// Swagger Doc
	"summary" => "Login a user",
	"PATH" => [],
	"GET" => [],
	"POST" => [
		["name" => "username", "description" => "The username to authenticate"],
		["name" => "password", "description" => "The password to authenticate"]
	]
	
]), function () use ($app) {

	// get request info
	$req = $app->request();

	// Get required parameters
	$username = $req->post('username');
	$password =  $req->post('password');

	// Init Auth object
	$auth = new \Bake\Auth();

	// Try to authenticate user
	$authInfo = $auth->authenticate($username, $password);

	// Add api key info to response
	$authInfo['data'][$app->config('apikey')->name] = $app->config('apikey')->val;
	
	// Try to authentication, return role information to user
	echo json_encode($authInfo);

})->name('login');



//---------------------------------
// Logout user
//---------------------------------
$app->get('/auth/logout',  $apiDoc->routeDoc('logout', [
// Swagger Doc
"summary" => "Logout a user",
"GET" => []
]),function () use ($app) {

	// Expire apikey on the server
	$_SESSION = array();
	session_unset();
	session_destroy();
	
	$params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );

	// Return empty
	echo json_encode(['data' => new stdClass(), 'meta' => ["msg" => "ok"]]);

})->name('logout');

// Refresh user role info from database
$app->get('/auth/roles', $acl->allow(['master', 'user']), function () use ($app) {

	// Init Auth object
	$auth = new \Bake\Auth();

	// Refresh role info into session and return info to user
	echo json_encode($auth->refreshRoles(session_id()));
});


//---------------------------------
// Start application
//---------------------------------
$app->run();
