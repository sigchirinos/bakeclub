/**
    Main router for the application, a controller of sorts.
    @module app/routers/main
    @extends Backbone.Router
 */
define([
  'backbone/backbone',
  'app/views/home',
  'app/views/events',
  'app/views/base',
  'app/models/note',
  'app/views/dash/storyinfo'
  ],
function (Backbone, HomeView, EventsView, BaseView, Note, StoryInfoView){
    /**
     * Constructor
     * @constructor
     * @alias module:app/routers/main
     */
    var exports = Backbone.Router.extend({
          /**
           * Uri route map
           * @type {Object}
           */
          routes: {
              "*actions"              : "defaultRoute"
          },
                
          /**
           * This function is automatically run on object instantiation
           * @param {object} options The initialization options
           */
          initialize: function (options) {
            // Set reference to base application view
            this.appView = options.appView;

            // Attach reference to router to base view prototype so that all views have access to the router
            BaseView.prototype.router = this;
            
            // Generate the base app view
            this.appView.render();
          },
          
          /**
           * This is the default route for all requestes to this router
           * @param {object} actions A piece of the uri from the routing
           */
          defaultRoute: function (actions){
            // Load Event home into dashboard
            this.appView.views['.main'].addView(new EventsView({title:'Events'}), true);
          },
          
          /**
           * Load a recipe card
           * @param {String} id The id number of the recipe to load.
           */
          loadNote: function (id){
            // Load Event home into dashboard
            this.appView.views['.main'].addView(new HomeView({title:'Events Home'}), false);

            var model = new Note({"id": id});

            // Bind to fetch event
            model.on("change", function () { 
              // Create new info view, passing this slide's model
              var infoView = new StoryInfoView({"model": model});
              
              // Add event info view to the dashboard
              this.appView.views['.main'].addView(infoView, true);

            }, this);

            model.fetch();
            
          }
  });

    return exports;
});