<?php 
/**
 * Events API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Events_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "events";
    
    /**
     * The table where tags are stored
     * @var string
     */
    public $tagNode = "event_tags";
    
    /**
     * Allowed fields for user requests
     * @var array
     */
    public $fields = array("club_id", "title", "short_desc", "long_desc", "event_date", "create_date", "creator", "tag");

    /**
     * Get the most recent 5 events on either side of today's date
     * 
     * @return mixed
     */
    public function getCurrentEvents() { 
        
        // Setup data connection
        $sql = new BK_Db_SQL('events', $this->query);
        
        // Get current date 
        $cur_date = time();

        
        
        // Build custom query
        $query_text = 'select * from events where UNIX_TIMESTAMP(event_date) <= ?';
        $query_text = "select events.id, events.title, events.short_desc, events.long_desc, events.event_date, events.location, media.path 
        from events, media where events.id = media.type_ent_id and media.type = 'event' where UNIX_TIMESTAMP(events.event_date) <= ?";
        
        // set parameters to query by
        //$params['event_date'] = $cur_date;
        $params = '';

        if ($sql->readCustom($query_text, $params)) {
          $result = array("data" => $sql->rows, "meta" => new stdClass);

          // Format the event date 
          $result = array_map(function($row) {$row['event_date'] = date('F j, Y', strtotime($row['event_date']));return $row;}, $result["data"]);

        } else {
          throw new Exception("Event not found", 500);
        }
        
         return $result;
    }

    /**
     * Get all events
     * 
     * @return mixed
     */
    public function getEvents() { 
        
        // Setup data connection
        $sql = new BK_Db_SQL('events', $this->query);
        
        // Build custom query
        $query_text = "select events.id, events.title, events.short_desc, events.long_desc, events.event_date, events.location, media.path 
        from events, media where events.id = media.type_ent_id and media.type = 'event'";
        
        // Get querystring info
        $get = $this->validateData($this->comm->request->get);

        // Set default pagination settings
        $get['page'] = (isset($get['page'])) ? $get['page'] : 1;
        $get['count'] = (isset($get['count'])) ? $get['count'] : 10;

        // set the pagination
        $sql->setPage($get['page'], $get['count']);
        
        if ($sql->readCustom($query_text)) {
          $result = array("data" => $sql->rows, "meta" => new stdClass);

          // Format the event date to something more friendly, for each row returned
          $result = array_map(
            function($row) {
                $row['event_date'] = date('F j, Y', strtotime($row['event_date']));
                return $row;
            }, $result["data"]);

        } else {
          throw new Exception("No events", 500);
        }
        
        return $result;
    }
    
    
}