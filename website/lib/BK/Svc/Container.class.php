<?php 
/**
 * Dependency injection container
 * 
 * Manages dependencies on objects by providing ability to register components and inject those components into objects
 * by providing speciic functions that create different kinds objects, a service locator essentially.
 * 
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Container {
    
    /**
     * This object properties array stores the dependencies to inject
     * @var array
     */
    private $prop;
    
    /**
     * Create an API using RESTFul communication interface
     *
     * @param string $class The name of the class to instantiate, inject stuff into and run
     * 
     * @return void
     */
    public function createRESTApi($class) 
    {
        // Init REST communication driver
        $commREST = new BK_Svc_REST;
        $commREST->setLog($this->prop['logger']);
        $commREST->setBasePath($this->prop['basePath']);
        
        // Init API, injecting the REST communication interface
        $api = new $class($commREST);
        $api->log = $this->prop['logger'];
        $api->query = $this->prop['query'];
        $api->routeMap = $this->prop['routeMap'];

        // Return API
        return $api;
    }
    
    /**
     * Set an object property
     * 
     * @param string $name the name of the property try to be set
     * @param string $value the value of the property try to be set
     * 
     * @return void
     */
    public function __set($name, $value) {
        $this->prop[$name] = $value;
    }
}