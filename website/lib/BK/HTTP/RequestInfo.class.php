<?php 
/**
 * Wrapper to facillitate parsing the request information from a HTTP request
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_HTTP_RequestInfo { 
    
    /**
     * The base path where the api is served from
     * @var string
     */
    public $basePath;
    
    /**
     * The parsed uri to be used as a routing path
     * @var string
     */
    public $routePath;
    
    /**
     * HTTP method
     * @var string
     */
    public $method;

    /**
     * Querystring variables 
     * @var array
     */
    public $get;


    /**
     * POST/PUT variables
     * @var array
     */
    public $post;
    
    /**
     * Run init operations
     * 
     * @return void  
     */
    public function __construct($opt = array()) {
        
        // set base path for api
        isset($opt['base_path']) ? $this->basePath = $opt['base_path'] : '';
        
        // parse request
        $this->parseRequest();
    }
    
    /**
     * Parse request info, breaking down into the basic parts we need
     * 
     * @return void  
     */
    public function parseRequest() {
        // parse url path from full uri, this removes filenames if in the uri
        $this->routePath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); 
        
        // Strip base path
        $this->routePath = str_replace($this->basePath, "",  $this->routePath);
       
        // get request method
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);  
        
        // Store payload info for each HTTP method type
        if ($this->method == "GET") {
            $this->get = $_GET; 
        } else if ($this->method == "POST") {
            $this->post = empty($_POST) ? json_decode(file_get_contents('php://input'), TRUE) : $_POST; 
            
            // check if files were uploaded
            if (sizeof($_FILES)) {
              $this->files = $_FILES;
            }
            
        } else if ($this->method == "PUT") {
            $this->post = json_decode(file_get_contents('php://input'), TRUE);
        }
            
    }
    
}