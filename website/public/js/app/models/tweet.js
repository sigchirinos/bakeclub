define(['backbone/backbone'], function(Backbone){
    
    /**
     * Tweet
     */
    var Tweet = Backbone.Model.extend({
        parse : function(response) {
            var data = {};

            // Check for "data" property, means we are getting it back directly from server
            if(response.data) {
                data = response.data;
            } else {
                data = response;
            }

            // Create client unique identifier
            data.client_ident = "tweet" + data.id;

            return data;
        }
    });

    return Tweet;
});