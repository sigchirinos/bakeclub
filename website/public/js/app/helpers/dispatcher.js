/** 
    Handles coordinating events across the application
    @module app/mods/dispatcher
    @extends Backbone.Events
 */
define(['backbone/backbone'], 
function(Backbone){

	return _.clone(Backbone.Events);

});