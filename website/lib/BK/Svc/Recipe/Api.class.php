<?php 
/**
 * Recipe API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Recipe_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "recipes";
    
    /**
     * Get a list of recipes
     * 
     * @return mixed
     */
    public function getRecipes() {
        
        // Setup query connection
        $sql = new BK_Db_SQL('recipes', $this->query);
        
        // get range from query string
        $limit = filter_var($this->comm->request->get['limit'], FILTER_VALIDATE_INT);
        $page = filter_var($this->comm->request->get['page'], FILTER_VALIDATE_INT);       
       
        if ($limit) {
            $sql->limit("LIMIT $limit");
        } else {
            // default to 10
            $sql->limit('LIMIT 10');
        }
               
        // Query db
        if ($sql->readAll()) {
            $result = array("data" => $sql->rows, "meta" => new stdClass);
        } else {
            // return error message
            $result = array("data" => new stdClass, "meta" => array("code" => 500, "msg" => "Recipe not found"));
        }
       
        return $result;
    }
}