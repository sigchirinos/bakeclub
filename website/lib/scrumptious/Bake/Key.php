<?php 
/**
 * Session handling with apikeys
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
namespace Bake;

class Key {

     /**
     * The name of the apikey session
     * @var mixed
     */
    public $name;

    /**
     * The value of the apikey session
     * @var mixed
     */
    public $val;

    /**
     * Object relational mapper used to interact with database
     * @var mixed
     */
    public $orm;

    /**
     * How long the keys will last
     * @var int
     */
    public $expires;


	/**
     * Init stuff
     * 
     * @param ORM $orm Idiorm object to connect to database
     * @param string $name the name of the apikey
     * @param int $expiration expiration time of the apikey in seconds
     * 
     * @return void  
     */
    public function __construct($name = 'apikey', $expiration = 21600, $orm = null) {
        // Set expiration 
        $this->expires = $expiration;

        // Set ORM object to connect to db
        $this->orm = (empty($orm) ? \ORM::for_table('apikeys') : $orm);
        
    	// Set name of session, used as cookie name
    	$this->name = $name;
    	session_name($this->name);

        // Check if session/apikey was passed in querystring or header
        $ext_id = '';
        if (isset($_GET['apikey'])) {
            $ext_id = $_GET['apikey'];
        } elseif (isset($_SERVER['HTTP_X_APIKEY'])) {
            $ext_id = $_SERVER['HTTP_X_APIKEY'];
        }
        
        // Filter externally provided apikey
        if (!empty($ext_id)) {
            $ext_id = filter_var($ext_id, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9]+$/")));

            // Resume using existing session
            if ($ext_id !== false) {
                session_id($ext_id);
            }
        } 

        // Set the session's cookie parameters
        session_set_cookie_params($this->expires);

        // Register this object as the session handler
        session_set_save_handler( 
            array( &$this, "open" ), 
            array( &$this, "close" ),
            array( &$this, "read" ),
            array( &$this, "write"),
            array( &$this, "destroy"),
            array( &$this, "gc" )
        );

        // Start session handling
        session_start();

        // Store key value
        $this->val = session_id();
    }

    /**
     * Opens the session
     * 
     * @param string $path path where session info is stored. (uneeded when using db)
     * @param string $name name of the session
     * @return boolean
     */
    public function open($path, $name) 
    {
    	return true;
    }


    /**
     * Close the session
     * 
     * @return boolean
     */
    public function close() 
    {
    	// Since we are using a db we technically don't need anything in this function
    	// If we were using files this is where we would close the file handles

    	return true;
    }


    /**
     * Read session data
     * 
     * @param string $id unique identifier for the session
     * @return boolean
     */
    public function read($id) 
    {	
        // Read session info from db
        $session = $this->orm->find_one($id);

    	// Did we find the session?
    	if ($session !== false) {
           
            // Check if session has expired?
            if ($session->access < (time() - $session->expires)) {
               
                // Since data is expired give them nothing back and remove old session
                $this->destroy($id); 
                return '';
            } else {
                // Return session data to them
                return $session->data;
            }
    	} else {
            
    		// Create new session data
            $this->orm->create();
            $this->orm->id = $id;
            $this->orm->created = time();
            $this->orm->access = time();
            $this->orm->expires = $this->expires;
            $this->orm->client_ip = $_SERVER['REMOTE_ADDR'];
            $this->orm->user_agent = $_SERVER['HTTP_USER_AGENT'];

            // Save to db
            $this->orm->save();

    		// Return empty string
    		return '';
    	}
    }


    /**
     * Write session data
     * 
     * @param string $id unique identifier for the session
     * @param string $data session data
     * @return boolean
     */
    public function write($id, $data) 
    {   
        // Check if userid was passed and store that in a seprate column
        // we use this later for faster lookups when retrieving access control information
        if (isset($_SESSION['user_id'])) {
            $this->orm->user_id = $_SESSION['user_id'];
        }

        // Set session data to write
        $this->orm->id = $id;
        $this->orm->data = $data;
        $this->orm->access = time();

        // Save to db
        return $this->orm->save();
    }


    /**
     * Destory a session
     * 
     * @param string $id unique identifier for the session
     * @return boolean
     */
    public function destroy($id) 
    {
        $this->orm->where('id', $id)->find_one()->delete();
        return true;

    }


    /**
     * Clean out old expired sessions
     * 
     * @param string $expires how many seconds a session lasts
     * @return boolean
     */
    public function gc($expires) 
    {
        // Build query to remove old sessions
        // Calculate the expiration time of a session
        // sessions are only valid between now and going back how long a sessions lasts
        // we are using are own expiration in the database and ignoring the value php passes
        return $this->orm->raw_query('DELETE FROM apikeys WHERE access < (:exp - expires)', ['exp' => time()])->delete();
    }


    /**
     * Destory object
     * 
     * @return void
     */
    public function __destruct()
    {
		// When using objects as session save handlers, it is important to register the shutdown function with 
		// PHP to avoid unexpected side-effects from the way PHP internally destroys objects on 
		// shutdown and may prevent the write and close from being called.
		register_shutdown_function('session_write_close');
	
	}
}