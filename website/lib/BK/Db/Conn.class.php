<?php 
/**
 * Database connection object
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Db_Conn
{
    /**
     * Open connection to a database and return connection object, defaults to mysqli and utf-8 connection charset
     *
     * @param string $dbhost hostname of database to connect to
     * @param string $dbusername username to connect as
     * @param string $dbpassword password to authenticate 
     * @param string $dbname name of database to use when connection
     * 
     * @return mixed  
     */
    public static function open($dbhost, $dbusername, $dbpassword, $dbname = ""){   
        // conect to database
        $dbconn = new mysqli($dbhost, $dbusername, $dbpassword, $dbname);
        
        // check connection
        if (mysqli_connect_errno()) {
            $dbconn = false;
        } else {
            $dbconn->set_charset("utf8"); // set charset
        }
        
        // return connection object
        return $dbconn;
    }// end open
}