<?php 
// Init framework autoloader
require '../lib/BK/Bootstrap.php';

// Init Dependency Container
$container = new BK_Svc_Container();

// The root folder for api access
$container->basePath = '/api';

// DB access object
$container->query = new BK_Db_Query(BK_Config::$DB);

// Session handling
$container->apiKey = new BK_Svc_Key();

// Logging
$container->logger = new BK_Util_KLogger(BK_Config::$LOGPATH.'api/media', BK_Util_KLogger::OFF);

// Route map controls uri mapping to api function calls, as well as permissions
$container->routeMap = array(
        '@^/media/([0-9]+)$@'          => array("GET"      => "getMedia", 
                                               "PUT"      => "update", 
                                               "DELETE"   => "delete"),
                                         
        '@/media$@'                     => array("POST"     => "addMedia", "GET" => 'getRecentMedia'),                                 
        '@/media/event/([0-9]+)$@'      => array("GET"      => "getMediaByEvent"),
        '@/media/tags/([0-9]+)$@'       => array("GET"      => "getMediaTags")
        
    );

// Run api
$api = $container->createRESTApi('BK_Svc_Media_Api');
$api->run();