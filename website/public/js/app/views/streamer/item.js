/**
    Represents a single item in a streamer
    @module app/views/streamer/item
    @extends app/views/base
 */
define([
        'app/views/base',
        'app/views/dash/storyinfo',
        'text!app/tpl/streamer/item.html'
        ],
function(BaseView, StoryInfoView, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias module:app/views/streamer/item
     */
    var exports = BaseView.extend({
        /**
            HTML tag to use for the view's root element.
            @type {String}
            @default
         */
        tagName : 'li',

        /**
            HTML class for the view's root element.
            @type {String}
            @default
         */
        className : '',

        /**
         * Specifies the event handlers and which functions the wire up to.
         * @type {Object}
         */
        events : {
            "click" : "displayDetail"
        },

        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);
        },

        /**
         * Get full detail of stream item from server
         * @param {Function} callback The function to execute upon successfull fetching of info from server
         */
        getDetail : function (callback) {
            // Get full model info from server
            // silent true means that we don't trigger a change event on the model
            this.model.fetch({silent: true, success: callback});
        },

        /**
         * Display the full details of a stream item
         * @param {Event} e The object for the fired event
         */
        displayDetail : function (e) {
            // capture clicks
            e.preventDefault();

            // Get full data
            var self = this;
            this.getDetail(function () {

                // Create new info view, passing this slide's model
                var infoView = new StoryInfoView({model: self.model});

                // Add event info view to the dashboard
                self.router.app.views['.main'].addView(infoView, true);
            });
        }
    });

    return exports;
});