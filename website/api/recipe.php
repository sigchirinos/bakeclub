<?php 
// Init framework autoloader
require '../lib/BK/Bootstrap.php';

// Init Dependency Container
$container = new BK_Svc_Container();

// The root folder for api access
$container->basePath = '/api';

// DB access object
$container->query = new BK_Db_Query(BK_Config::$DB);

// Session handling
$container->apiKey = new BK_Svc_Key();

// Logging
$container->logger = new BK_Util_KLogger(BK_Config::$LOGPATH.'api/recipe', BK_Util_KLogger::OFF);

// Route map controls uri mapping to api function calls, as well as permissions
$container->routeMap = array(
        '@^/recipe/([0-9]+)@'   => array(  "GET"         => "read", 
                                            "PUT"         => "update", 
                                            "DELETE"      => "delete"),
                                         
        '@^/recipe$@'            => array( "POST"        => "create"),
        '@^/recipe/list$@'       => array( "GET"         => "getRecipes")
    );

// Run api
$api = $container->createRESTApi('BK_Svc_Recipe_Api');
$api->run();