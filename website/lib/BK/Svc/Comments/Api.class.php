<?php 
/**
 * Comments API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Comments_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "comments";

    /**
     * Get the most recent comments
     * 
     * @return mixed
     */
    public function getRecentComments() {
      return array('data' => array("title" => 'Most recent comment'), 'meta' => new stdClass);
    }
     
}