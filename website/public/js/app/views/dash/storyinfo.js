/** 
    Display's full information for a news story
    @module app/views/dash/storyinfo
    @extends app/views/base
 */
define([
    'app/views/base',
    'text!app/tpl/dash/storyinfo.html'
    ], 
function(BaseView, Tpl){
    /** 
     * @constructor
     * @alias module:app/views/dash/storyinfo
     */
    var exports = BaseView.extend({

        /** 
            HTML tag to use for the view's root element.
            @type {String}
            @default
         */
        tagName: 'div',

        /** 
            HTML class for the view's root element.
            @type {String}
            @default
         */
        className: 'dash-item',

        /**
         * Automatically called upon object construction. 
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);
        },

    });

    return exports;
});