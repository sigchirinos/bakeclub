<?php 
// Bootstrap application
require 'bootstrap.php';

// This generates the base swagger documentatino json
// that points to where the documentation endpoints are for all other apis
$app->get('/doc', function () use ($app) {
	// Generate base api info
	$apiData['apiVersion'] = "0.1";
	$apiData['swaggerVersion'] = "1.2";
	$apiData['basePath'] = "http://localhost:8888/api/";

	// Add the paths for each api's documentation
	$apiData['apis'][] = ["path" => "/auth/api-doc/auth", "description" => "This is the Auth API"];
	$apiData['apis'][] = ["path" => "/users/api-doc/users", "description" => "This is the Users API"];

	// output as json
	$app->response()['Content-Type'] = 'application/json';
	echo json_encode($apiData, JSON_UNESCAPED_SLASHES);
});

// Start application
$app->run();