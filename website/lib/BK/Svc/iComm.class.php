<?php 
/**
 * Interface for creating communications drivers. Implement this interface when create communication drivers for 
 * web services such as REST or Thrift or SOAP etc...
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
interface BK_Svc_iComm {
    
    /**
     * Set the API object to execute functions on
     * 
     * @param array $api object containings the api functions to execute
     * 
     * @return void
     */
    public function setApi($api);
    
    /**
     * Set the logging object to use
     * 
     * @param object $logger logging object
     * 
     * @return void
     */
    public function setLog($logger);
    
    /**
     * Process API request
     * 
     * @return void
     */
    public function run();
    
}