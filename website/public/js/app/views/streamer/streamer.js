/**
    Streaming list of items from the server. You can various kinds of items to a single streamer.
    @module app/views/streamer/streamer
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/views/streamer/item',
    'app/views/streamer/item_note',
    'app/views/streamer/item_tweet',
    'app/views/streamer/item_talk',
    'app/views/streamer/item_media',
    'text!app/tpl/streamer/streamer.html',
    'text!app/tpl/streamer/streamer_simple.html'
    ],
function(BaseView, StreamItemView, NoteItemView, TweetItemView, TalkItemView, MediaItemView, Tpl, TplSimple){
    /**
     * Contructor
     * @constructor
     * @alias module:app/views/streamer/streamer
     */
    var exports = BaseView.extend({

        /**
         * Maintains collections not currently viewed
         * @type {Object}
         */
        sources : {},

        /**
         * Stores collections for the view
         * @type {Object}
         */
        collections : {},

        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // Compile template
            if (this.options.template && this.options.template === 'simple') {
                this.template = _.template(TplSimple);
            } else {
                this.template = _.template(Tpl);
            }

            // Set collections to use for streams
            if(this.options.collections) {
                this.collections = this.options.collections;
            }

            // Attach listeners to collections events
            for (var coll in this.collections) {
                // Listen to reset to do mass updates to UI
                this.collections[coll].on("reset", function(self, collName) {
                    return function (collection) {self.addMany(collection, collName);};
                }(this, coll), this);

                // Get data from server now that UI is listening for updates
                this.collections[coll].fetch();

                // Listen to add for bite size UI updates
                this.collections[coll].on("add", function(self, collName) {
                    return function (item) {self.addItem(item, collName);};
                }(this, coll), this);
                
            }

        },
        // Wire up events
        events : {
            
        },

        /**
         * Add multiple items
         *
         * @returns undefined
         */
        addMany : function(collection, type) {
            // Iterate through the collection and add each item
            for(var item in collection.models) {
                this.addItem(collection.models[item], type);
            }
        },

        /**
         * Add new event to the event slider view
         * @param {Bake.SlideItem} item the event to add to the slider
         */
        addItem : function(item, type) {

            // Create new stream item
            var view;
            
            // render the item into the correct stream list
            if(type == 'note') {
                view = new NoteItemView({model : item, className: type});
                $('ul.note', this.$el).append(view.render().el);
            } else if(type == 'tweet') {
                view = new TweetItemView({model : item, className: type});
                $('ul.tweet', this.$el).append(view.render().el);
            } else if(type == 'talk') {
                view = new TalkItemView({model : item, className: type});
                $('ul.talk', this.$el).append(view.render().el);
            } else if(type == 'media') {
                view = new MediaItemView({model : item, className: type});
                $('ul.media', this.$el).append(view.render().el);
            }

        },

        /**
         * Changes the source urls for all collections and refetch them, stores the old collection info
         * @param {Int} id source event to gather info for
         */
        changeSource : function(id) {
                 
                // Create copy of collection
                var collections = this.collections;
                
                // Iterate through collection and set source url
                for(var col in this.collections) {
                    // Clear out old items from display
                    $('ul.'+col, this.$el).empty();
                    
                    // Get new items
                    this.collections[col].fetch({
                        url:this.collections[col].url+'/'+id,
                        success: function(){
                            
                        }
                    });
                }
            
        }
    });

    return exports;
});