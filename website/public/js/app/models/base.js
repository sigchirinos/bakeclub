/**
    A base model that all models in the app derive from. Implements some shared functionality
    that the base Backbone models don't provide but our useful to this app.
    @module app/models/base
    @extends Backbone.Model
 */
define([
    'backbone/backbone'
    ],
function(Backbone){
    /**
     * Constructor
     * @constructor
     * @alias module:app/models/base
     */
    var exports = Backbone.Model.extend({
        /**
         * This function is called whenever a model requests data from the server. Is used to modify the response
         * before it is set to the object.
         * @param {String} response A json response object that is returned from the model's call to the server.
         */
        parse : function(response) {
            var data = {};

            // Check for "data" property, means we are getting it back directly from server
            if(response.data) {
                data = response.data;
            } else {
                data = response;
            }

            return data;
        }
    });

    return exports;
});