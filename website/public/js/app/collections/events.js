define(['backbone/backbone', 'app/models/event'], function(Backbone, Event){
    
    /**
     * Events
     */
    var Events = Backbone.Collection.extend({

        url : '/api/events',

        model : Event,

        initialize : function() {

        },
        parse : function(response) {
            // Check for "data" property, means we are getting it back directly from server
            if(response.data) {
                return response.data;
            } else {
                return response;
            }
        }
    });

    return Events;
});