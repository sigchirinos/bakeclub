/**
    The main dashboard view of the application.
    @module app/views/home
    @extends app/views/base
 */
define([
    'app/views/base',
    'app/collections/events',
    'app/views/slider/slider',
    'text!app/tpl/events.html'
    ],
function(BaseView, EventsColl, SliderView, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias module:app/views/home
     */
    var exports = BaseView.extend({
        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // Call parent
            BaseView.prototype.initialize.call(this);

            // create new events collection
            var events = new EventsColl();

            // Compile template
            this.template = _.template(Tpl);

            // Add child views, passing events to slider
            this.views['#main-left'] = new SliderView({collection: events});
        }
    });

    return exports;
});