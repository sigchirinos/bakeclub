<?php 
/**
 * A RESTFul web service driver. Handles RESTFul HTTP communication for an api.
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_REST implements BK_Svc_iComm {

    /**
     * The API object which holds the functions to execute when matched from the routing map
     * @var mixed
     */
    protected $api;

    /**
     * Array of access level codes.
     * @var array
     */
    protected $acccessLevels = array(0 => array(), 1 => array("GET"), 2 => array("GET", "POST", "PUT", "DELETE"));

    /**
     * The base uri path where the api is served from
     * @var mixed
     */
    protected $basePath;
    
    /**
     * Logging object
     * @var mixed
     */
    protected $log;

    /**
     * Whether to require ssl to connect to service
     * @var boolean
     */
    public $forceSSL = false;
    
    /**
     * Request information
     * @var mixed
     */
    public $request;
    
    /**
     * Response information to be sent to client
     * @var mixed
     */
    public $response;

    /**
     * Constructor
     * 
     * @return void  
     */
    public function __construct() {}


    /**
     * Process API request
     * 
     * @return void
     */
    public function run() {
        
        // Try routing request
        try {
            $this->routeRequest();
        } catch (Exception $e) {
            // set response to error condition
            $this->setResponse(array(
                                    "data" => new stdClass(),
                                    "meta" => array("code" => $e->getCode(), "msg" => $e->getMessage())
                                    )
                              );
        }
        
        // Output response to user
        $this->sendResponse(); 
    }

    /**
     * Routes the request to the proper class function
     * 
     * @return void
     */
    public function routeRequest() {
        
        // get request info
        $this->request = new BK_HTTP_RequestInfo(array("base_path" => $this->basePath));        

        // Check where routing info is in route map
        if (isset($this->api->routeMap['routes'])) {
            // Extract routes from array
            $routes = $this->api->routeMap['routes'];            
        } else {
            // No "routes" key, means that the array only contains routing info
            $routes = $this->api->routeMap;
        }

        // Iterate through route map until match is found
        foreach($routes as $uri => $value){
         
           if (preg_match($uri, $this->request->routePath, $matches) === 1) {
                // Route found! First we need to check the permissions
                if ($this->checkPermissions($uri) !== true) {
                    throw new Exception('Unathorized', 401);
                }
              
               // Check if we have a function mapped to the request method
               if (array_key_exists($this->request->method, $value)) {
                   $func_name = $value[$this->request->method];
               } else { 
                  // Route exists for uri, but the request method didn't match what route specified               
                  throw new Exception('Method not mapped.', 400);
               }
               
               // Init arguments
               $args = array();
               
               // For GET, PUT and DELETE may specify uri arguments
               // Argument comes from regex matches array backreferences 
               if ($this->request->method == 'GET' || $this->request->method == 'DELETE' || $this->request->method == 'PUT') {
                   // shift off the first element because that is just the whole regex match
                  array_shift($matches);
                  $args = $matches;
               }
                
                // Try to execute function 
                try {
                  
                  // Get result and set to response body
                  $result = call_user_func_array(array($this->api, $func_name), $args); 
                  $this->setResponse($result);
                  
                } catch (Exception $e) {
                  
                  // Pass exception up the stack
                  throw $e;
                }
               
               // exit loop and give control back to caller
               return TRUE;
           } 
           
        }
                         
       // Couldn't find the uri in the route map               
       throw new Exception('Uri route not found.', 400);
    }
    
    
    /**
     * Checks if user has access to the uri requested
     * @param string $uriKey the matched uri key from the routemap to check permissions on
     * @return boolean 
     */
    private function checkPermissions($uriKey) 
    {
        // Check if perms are defined for the api
        if (!empty($this->api->routeMap['perms']) && is_array($this->api->routeMap['perms'])) {
            // Extract permissions array
            $perms = $this->api->routeMap['perms'];

            // Since there is someting set in perms array 
            // we insitute a deny all access policy as the default
            $access_level = 0;

            // First we check default permissions for all uris
            if (!empty($perms['default']) && is_array($perms['default'])) {

                // Does user have any roles set?
                if (!empty($_SESSION['roles']) && is_array($_SESSION['roles'])) {

                    // Check to see if user's roles match those in default perms array
                    $allowed = array_intersect_key($perms['default'], $_SESSION['roles']);

                    // Did user have any matching roles?
                    if (!empty($allowed)) {
                        // Sort by access level
                        asort($allowed);

                        // Get the highest access level defined
                        $access_level = array_pop($allowed);
                    } else {
                        // No matching roles so user default role
                        if (isset($perms['default']['default'])) {
                            $access_level = $perms['default']['default'];
                        }
                    }

                } else {
                    // User has no roles set, so try getting access level from default permission
                    if (isset($perms['default']['default'])) {
                        $access_level = $perms['default']['default'];
                    } 
                } 
            }

            // Next we see if there are permissions set on the uri specifically
            // They override default perms
            if (!empty($perms[$uriKey]) && is_array($perms[$uriKey])) {

                // Does user have any roles set?
                if (!empty($_SESSION['roles']) && is_array($_SESSION['roles'])) {
                   
                    // Check to see if user's roles match those in perms array
                    $allowed = array_intersect_key($perms[$uriKey], $_SESSION['roles']);
                    
                    // Did user have any matching roles?
                    if (!empty($allowed)) {
                        // Sort by access level
                        asort($allowed);

                        // Get the highest access level defined
                        $access_level = array_pop($allowed);
                    } else {
                        // No matching roles so user default role
                        if (isset($perms[$uriKey]['default'])) {
                            $access_level = $perms[$uriKey]['default'];
                        }
                    }
                } else {
                    // User has no roles matching, so try getting access level from default permission
                    if (isset($perms[$uriKey]['default'])) {
                        $access_level = $perms[$uriKey]['default'];
                    } 
                }
            }

            // We've extracted the access level, now see if they are allowed to execute the HTTP method type
            if (array_key_exists($access_level, $this->acccessLevels)) {
                if (in_array($this->request->method, $this->acccessLevels[$access_level])) {
                    // Allow access
                    return true;
                } else {
                    // Deny access
                    return false;
                }
            } else {
                // The access level is not defined, so deny access.
                return false;
            }

           
        } else {
            // No perms set so allow unfettered access
            return true;
        }
    }


    /**
     * Set the response object
     * 
     * @param array $info result data from function exection to be encoded and output to user
     * 
     * @return void
     */
    protected function setResponse($info) {
        
        // Create response object
        $this->response = new BK_HTTP_Response();
        
        // Set status code
        if (isset($info['meta']) && is_array($info['meta']) && isset($info['meta']['code'])) {
           $this->response->setStatus($info['meta']['code']); 
        }
        
        // Encode response and set body
        $this->response->body = json_encode($info);
        
        // set json content type header
        $this->response->headers['Content-type'] = 'application/json';
    }
    
    /**
     * Outputs response info to user
     * 
     * @return void
     */
    protected function sendResponse() {
        // set status code
        if (isset($this->response->statusCode)) { 
            header('HTTP/1.0 '.$this->response->statusCode.' '.$this->response->status, TRUE, $this->response->statusCode);
        }
       
        // send headers
        if (is_array($this->response->headers)) {
            foreach ($this->response->headers as $key => $value) {
                header($key.":".$value);
            } 
        }
       
        // send body
        echo $this->response->body;
       
        // stop all execution, so nothing else happens
        exit();
    }
    
    /**
     * Set the base path for the api to assit in uri routing
     * 
     * @param string $basePath a uri path
     * 
     * @return void
     */
    public function setBasePath($basePath) {
        $this->basePath = $basePath;
    }
    
    
    /**
     * Set the API object to execute functions on
     * 
     * @param array $api object containings the api functions to execute
     * 
     * @return void
     */
    public function setApi($api) {
        $this->api = $api;
    }
    
    /**
     * Set the logging object to use
     * 
     * @param array $logger logging object
     * 
     * @return void
     */
    public function setLog($logger) {
        $this->log = $logger;
    }
    
}