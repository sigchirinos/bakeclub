<?php 
// Init framework autoloader
require '../lib/BK/Bootstrap.php';

// Init Dependency Container
$container = new BK_Svc_Container();

// The root folder for api access
$container->basePath = '/api';

// DB access object
$container->query = new BK_Db_Query(BK_Config::$DB);

// Session handling
$container->apiKey = new BK_Svc_Key();

// Logging
$container->logger = new BK_Util_KLogger(BK_Config::$LOGPATH.'api/notes', BK_Util_KLogger::OFF);

// Route map controls uri mapping to api function calls, as well as permissions
$container->routeMap = array(
        "routes"  => array(
            '@^/notes/([0-9]+)@'   => array(
                                            "GET"      => "read", 
                                            "PUT"      => "update", 
                                            "DELETE"   => "delete"
                                            )
                                             
            ,'@/notes$@'             => array(
                                              "POST"     => "addNote",
                                              "GET"      => "getNotes"
                                              )
                                            
            ,'@/notes/event/([0-9]+)$@'         => array("GET"  => "getEventNotes")
            ,'@/notes/tweets$@'                 => array("GET"  => "getRecentTweets")
          ),
        
        "perms"   => array(
          // 1 = read access, 2 = read/write access
          "default"     => array("user" => 1, 'default' => 1),
          "@/notes$@"   => array('default' => 1)
        )                               
	);

// Run api
$api = $container->createRESTApi('BK_Svc_Notes_Api');
$api->run();