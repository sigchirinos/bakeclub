<?php 
/**
 * Manages keys for access to web service
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Key {

     /**
     * The name of the apikey session
     * @var mixed
     */
    public $name;

    /**
     * The value of the apikey session
     * @var mixed
     */
    public $val;

    /**
     * Query object usd to interact with database
     * @var mixed
     */
    public $query;

    /**
     * The table or node where info is stored
     * @var mixed
     */
    public $node = 'apikeys';

    /**
     * How long the keys will last
     * @var int
     */
    public $expires;


	/**
     * Init stuff
     * 
     * @param object $keyType type of api key to use, ex: cookie, header
     * @param object $keyName the name of the apikey
     * 
     * @return void  
     */
    public function __construct($name = 'apikey') {
        // Set expiration 
        $this->expires = BK_Config::$KEYEXP;
        
    	// Set name of session, used as cookie name
    	$this->name = $name;
    	session_name($this->name);

        // Check if session/apikey was passed in querystring or header
        $ext_id = '';
        if (isset($_GET['apikey'])) {
            $ext_id = $_GET['apikey'];
        } elseif (isset($_SERVER['HTTP_X_APIKEY'])) {
            $ext_id = $_SERVER['HTTP_X_APIKEY'];
        }
        
        // Filter externally provided apikey
        if (!empty($ext_id)) {
            $ext_id = filter_var($ext_id, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9]+$/")));

            // Resume using existing session
            if ($ext_id !== false) {
                session_id($ext_id);
            }
        } 

        // Set the session's cookie parameters
        session_set_cookie_params($this->expires);

    	// Register this object as the session handler
		session_set_save_handler( 
			array( &$this, "open" ), 
			array( &$this, "close" ),
			array( &$this, "read" ),
			array( &$this, "write"),
			array( &$this, "destroy"),
			array( &$this, "gc" )
		);

		// Start session handling
		session_start();

        // Store key value
        $this->val = session_id();
    }


    /**
     * Opens the session
     * 
     * @param string $path path where session info is stored. (uneeded when using db)
     * @param string $name name of the session
     * @return boolean
     */
    public function open($path, $name) 
    {
    	// Init query object to access database
    	$this->query = new BK_Db_Query(BK_Config::$DB);

    	return true;
    }


    /**
     * Close the session
     * 
     * @return boolean
     */
    public function close() 
    {
    	// Since we are using a db we technically don't need anything in this function
    	// If we were using files this is where we would open the file handles

    	return true;
    }


    /**
     * Read session data
     * 
     * @param string $id unique identifier for the session
     * @return boolean
     */
    public function read($id) 
    {	
    	// Setup sql object to keys table
    	$sql = new BK_Db_SQL($this->node, $this->query); 
        
    	// Read session info from db
    	if ($sql->read(array('id' => $id))) {

            // Check if session has expired?
            if ($sql->rows[0]['access'] < (time() - $sql->rows[0]['expires'])) {
                // Since data is expired give them nothing back and remove old session
                $this->destroy($id); 
                return '';
            } else {
                return $sql->rows[0]['data'];
            }
    	} else {
    		// Write empty session data
    		$result = $sql->create(array('id' => $id, 'created' => time(), 'access' => time(), 'expires' => $this->expires, 'client_ip' => $_SERVER['REMOTE_ADDR'], 'user_agent' => $_SERVER['HTTP_USER_AGENT']));
    		
    		// Return empty string
    		return '';
    	}

    	
    }


    /**
     * Write session data
     * 
     * @param string $id unique identifier for the session
     * @param string $data session data
     * @return boolean
     */
    public function write($id, $data) 
    {
    	// Setup sql object to keys table
    	$sql = new BK_Db_SQL($this->node, $this->query); 

    	// Setup params
    	$params['data'] = $data;
    	$params['access'] = time();

        // Check if userid was passed and store that in a seprate column
        // we use this later for faster lookups when retrieving access control information
        if (isset($_SESSION['user_id'])) {
            $params['user_id'] = $_SESSION['user_id'];
        }

    	// Write session info into db
    	$sql->update($id, $params);

    	// Check that update completed
    	if ($sql->query->affectedrows >= 0) {
    		return true;
    	}

    	// Writing to db failed for some reason
    	// TODO: log failure info
    	return false;
    }


    /**
     * Destory a session
     * 
     * @param string $id unique identifier for the session
     * @return boolean
     */
    public function destroy($id) 
    {	
    	// Setup query connection
        $sql = new BK_Db_SQL($this->node, $this->query);
        
        // Check if delete succeeded
        if ($sql->delete($id)) {
           return true;
        } else {
            return false;
        }
    }


    /**
     * Clean out old expired sessions
     * 
     * @param string $expires how many seconds a session lasts
     * @return boolean
     */
    public function gc($expires) 
    {
    	// Setup query connection
        $sql = new BK_Db_SQL($this->node, $this->query);

        // Build query to remove old sessions
        $query = 'DELETE FROM keys WHERE access < (? - expires)';

        // Calculate the expiration time of a session
        // sessions are only valid between now and going back how long a sessions lasts
        // we are using are own expiration in the database and ignoring the value php passes
        $params['currenttime'] = time();
        
        // Run query
        $sql->updateCustom($query);

        // Check if delete succeeded
        if ($sql->query->affectedrows >= 0) {
           return true;
        } else {
            return false;
        }
    }


    /**
     * Destory object
     * 
     * @return void
     */
    public function __destruct()
    {
		// When using objects as session save handlers, it is important to register the shutdown function with 
		// PHP to avoid unexpected side-effects from the way PHP internally destroys objects on 
		// shutdown and may prevent the write and close from being called.
		register_shutdown_function('session_write_close');
	
	}

}











