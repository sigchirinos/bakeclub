<?php 
/**
 * Web Service class parent class. API's created should derive from this base class. 
 * But they don't absolutely have to. This class provides a standard set of functionaility
 * that a web service might need.
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
abstract class BK_Svc_Base {
    
    /**
     * Communication interface driver. Handles how your api maps to a client communication method. REST, Thrist, SOAP, etc.
     * @var mixed
     */
    protected $comm;

    /**
     * Map used to figure out how to map requests to functions
     * @var array
     */
    public $routeMap = array();
    
    /**
     * Object to query a datastore with. Could by MySQL, Mongo, etc. Be familiar with implementation details to use properly in your APIs.
     * @var mixed
     */
    public $query;
    
    /**
     * Logging object
     * @var mixed
     */
    public $log;
    
    /**
     * The main table/node name that stores data for the service
     * @var string
     */
    public $node;
    
    /**
     * Where tags are stored for this kind of data
     * @var string
     */
    public $tagNode;
    
    /**
     * Allowed fields for user request
     * @var array
     */
    public $fields;
    
    /**
     * Tells whether to automatically send create_date when creating record
     * @var boolean
     */
    public $stampOnCreate = true;
    
    /**
     * Tells whether to automatically send update_date when updating record
     * @var boolean
     */
    public $stampOnUpdate = false;
   
    /**
     * Init stuff
     * 
     * @param object $commInterface the communication driver to use for handling client requests and responses
     * 
     * @return void  
     */
    public function __construct($commInterface) {
        $this->comm = $commInterface;
    }
    
    /**
     * Execute service using defined communication interface 
     * 
     * @return mixed
     */
    public function run() {
        // Pass the API into the context of the comm interface
        $this->comm->setApi($this);
        
        // This runs the API in the context of the communication interface
        // the output to the user is handled by the communication interface
        $this->comm->run();
    }  
   
    /**
     * Create a single record
     * 
     * @return mixed
     */
    public function create() {
      // Setup data connection
      $sql = new BK_Db_SQL($this->node, $this->query);
      
      // Validate request data
      $params = $this->validateData($this->comm->request->post);
     
      // Check if we need to create tags
      if (!empty($this->tagNode) && isset($params['tag'])) {
        $tags = $params['tag'];
        
        // need to remove tags from main params or generic sql call will fail 
        // because tags are in a different table
        unset($params['tag']);
      }
      
      // Check if we need to send create timestamp
      if ($this->stampOnCreate) {
        $params['create_date'] = 'NOW()';
      }
      
      // insert data
      if ($sql->create($params)) {
          // Get just inserted recipe to return to user
          $insert_data = $this->read($sql->query->insertid);

          // Store tags if we have them
          if (is_array($tags) && count($tags) > 0){
            $sql_tags = new BK_Db_SQL($this->tagNode, $this->query);
            foreach($tags as $value) {
              $sql_tags->create(array("media_id" => $insert_data['data']['id'], "tag" => $value));
            }
          }
          
          $result = array("data" => $insert_data['data'], "meta" => new stdClass);
      } else {
        throw new Exception("Failed to create.", 500);
      }
      
       return $result;   
    }
    
    /**
     * Get a single record
     * 
     * @param int $id Id number of record to retrieve
     * 
     * @return mixed
     */
    public function read($id) {
      // Setup query connection
      $sql = new BK_Db_SQL($this->node, $this->query);

      // Filter fields returned to user
      if (!empty($this->comm->request->get['fields']) && is_array($this->comm->request->get['fields'])) {
        $sql->fields = $this->filterFields($this->comm->request->get['fields']);
      }
     
      // Query db
      if ($sql->read(array('id' => $id))) {
          $result = array("data" => $sql->rows[0], "meta" => new stdClass);
      } else {
          throw new Exception('Not found', 400);
      }
      
      return $result;   
    }
    
    /**
     * Update a record
     * 
     * @return mixed
     */
    public function update($id) {
        
        // Setup query connection
        $sql = new BK_Db_SQL($this->node, $this->query);
        
        // Validate request data
        $params = $this->validateData($this->comm->request->post);
        
        // Check if we need to send create timestamp
        if ($this->stampOnUpdate) {
          $params['update_date'] = 'NOW()';
        }
        
        // Upate data
        $sql->update($id, $params);
        
        if ($sql->query->affectedrows >= 0) {
            // Return  data
            $data = $this->read($id);
            $result = array("data" => $data['data'], "meta" => new stdClass);
        } else {
            throw new Exception('Update failed', 500);
        }
        
        return $result;
    }
    
    /**
     * Delete a record
     * 
     * @return mixed
     */
    public function delete($id) {
        
        // Setup query connection
        $sql = new BK_Db_SQL($this->node, $this->query);
        
        // Check if update succeeded
        if ($sql->delete($id)) {
            $result = array("data" => new stdClass, "meta" => array("code" => 200, "msg" => "Delete successful"));
        } else {
            throw new Exception('Delete failed', 400);
        }
        
        return $result;
    }
    
    /**
     * Validdate fields before passing to 
     * 
     * @param array $data request data array
     * 
     * @return mixed
     */
    protected function validateData($data) {
      
      // rip out submit from params as we should never need it
      unset($data['submit']);
      
      return $data;
      
      // TODO Filter params to only contain allowed fields
      if (!empty($this->fields)) {
        foreach($this->fields as $field) {
          if (isset($params[$field])) {
            unset($params[$field]);
          }
        }
      }
      
    }
    
    /**
     * Validate fields before passing to 
     * 
     * @param array $fields
     * 
     * @return mixed
     */
    protected function filterFields($fields) {
      $filtered_fields = array();
      
      // Iterate through fields
      foreach($fields as $field) {
         
        // Check if field allowed
        if (in_array($field, $this->fields)) {
          $filtered_fields[] = $field;
        }
      }
      
      return $filtered_fields;
    }
    
    
}