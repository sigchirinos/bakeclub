<?php

// autoload_classmap.php generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'IdiormResultSet' => $baseDir . '/vendor/j4mie/idiorm/idiorm.php',
    'IdiormString' => $baseDir . '/vendor/j4mie/idiorm/idiorm.php',
    'IdiormStringException' => $baseDir . '/vendor/j4mie/idiorm/idiorm.php',
    'Model' => $baseDir . '/vendor/j4mie/paris/paris.php',
    'ORM' => $baseDir . '/vendor/j4mie/idiorm/idiorm.php',
    'ORMWrapper' => $baseDir . '/vendor/j4mie/paris/paris.php',
);
