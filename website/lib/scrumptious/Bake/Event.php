<?php 
/**
 * Event management library
 * @package Bake
 * @author Sigfrido Chirinos
 */
namespace Bake;

class Event {
    
    /**
     * The database abstraction object to query a database
     * @var mixed
     */
    public $orm;

    /**
     * Object settings
     * source - [db|ldap] The authentication source type 
     * @var array
     */
    public $settings = array();

    /**
     * Constructor
     * @param string $settings object settings
     */
    public function __construct($settings = array()) 
    {
        // Overwrite default settings
        $this->settings = array_merge([
            'orm' => \ORM::for_table('events')
            ], $settings
        );

        // connection to default database
        $this->orm = $this->settings['orm'];
    }

    /**
     * Get the most recent 5 events on either side of today's date
     * @return mixed
     */
    public function getCurrentEvents() { 
        
        // Get current date 
        $cur_date = time();

        // Build custom query
        $query_text = "select events.id, events.title, events.short_desc, events.long_desc, events.event_date, events.location, media.path 
        from events, media where events.id = media.type_ent_id and media.type = 'event' AND UNIX_TIMESTAMP(events.event_date) <= :date";
        
        // Query data base
        $query_result = $this->orm->raw_query($query_text, ['date' => $cur_date])->find_array();

        // Did we get back data?
        if ($query_result) {

            $result = array("data" => $query_result, "meta" => new \stdClass);
          
            // Format the event date 
            $result = array_map(function($row) {$row['event_date'] = date('F j, Y', strtotime($row['event_date']));return $row;}, $result["data"]);

        } else {
            throw new \Bake\AppException("Event not found", 500);
        }
        
         return $result;
    }     
}