/**
    A stream item for a note
    @module app/views/streamer/item_note
    @extends app/views/streamer/item
 */
define([
        'app/views/base',
        'app/views/streamer/item',
        'app/views/dash/storyinfo',
        'text!app/tpl/streamer/item_note.html'
        ],
function(BaseView, StreamItemView, StoryInfoView, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias module:app/views/streamer/item
     */
    var exports = StreamItemView.extend({
        /**
         * Automatically called upon object construction.
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);
        }
    });

    return exports;
});