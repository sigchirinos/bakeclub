<?php 
/**
 * Messaging API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Msg_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "msg_log";
    
    /**
     * Map used to figure out how to map requests to functions
     * @var array
     */
    public $routeMap = array(
        '@^/msg/([0-9]+)@'   => array("GET"      => "read", 
                                         "PUT"      => "update", 
                                         "DELETE"   => "delete"),
                                         
        '@/msg@'             => array("POST"     => "create")
    );
     
}