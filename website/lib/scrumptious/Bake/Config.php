<?php 
/**
 * The class is a global configuration settings container for all APIs.
 * You could also defined api specific configuration files within each API's folder.
 * @package     Bake
 */
namespace Bake;

class Config {
    /**
     * Database connection information
     * @var array
     */
    public static $DB = array(
        "bake-db1" => array(
            "host"              => "localhost", 
            "dbname"            => "bakeclub", 
            "user"              => "baker", 
            "password"          => "bakeclub"
        )   
    );

    /**
     * Filesystem and other path config info
     * @var array
     */
    public static $PATH = array(
      "upload-dir" => "/Users/sig/projects/bakeclub/website/public/img/",
      "base-dir" => "/Users/sig/projects/bakeclub/website"
    );

    /**
     * Filesystem and other path config info
     * @var string
     */
    public static $LIBPATH = '/Users/sig/projects/bakeclub/website/lib';

    /**
     * Path to lgos
     * @var array
     */
    public static $LOGPATH = '/Users/sig/projects/bakeclub/logs/';

    /**
     * Key/session expiration in seconds
     * @var int
     */
    public static $KEYEXP = 21600;

    /**
     * Configure Idiorm named connection info
     * Doing this here allows us to later just specify the connection name for the ORM object we create
     */
    public static function prepareDB() {
        // Setup database configuration for bakeclub database
        \ORM::configure('mysql:host='.self::$DB['bake-db1']['host'].';dbname='.self::$DB['bake-db1']['dbname'], null);
        \ORM::configure('username', self::$DB['bake-db1']['user']);
        \ORM::configure('password', self::$DB['bake-db1']['password']);
        \ORM::configure('return_result_sets', true);
    }
}