<?php 
/**
 * Users API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Users_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "users";

    /**
     * Add a new user 
     * 
     * @return boolean
     */
    public function addUser() 
    {
        // Filter Uername
        $username = filter_var($this->comm->request->post['username'], FILTER_VALIDATE_EMAIL);
        $is_email = true;

        // If failed email validation then try regular username
        if ($username === false) {
            $username = filter_var($this->comm->request->post['username'], FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9\.\_]+$/")));
            $is_email = false;
        }

        // TODO: filter password with greater range of options maybe
        $password = $this->comm->request->post['password'];

        // Did username and password check out
        if ($username !== false && $password !== false) {
            
            // Since we will update two tables, setup a transaction
            $this->query->beginTran();    

            // Setup data connection to auth table first
            $sql = new BK_Db_SQL('auth', $this->query);

            // Parameter for Auth table
            $params['username'] = $username;
            $params['password'] = BK_Util_Bcrypt::hash($password);

            // Add user authentication info
            if ($sql->create($params)) {
                
                // Get the id from the insert
                $id = $this->query->insertid;

                // Now we need to add the new user to the user info table
                $sql_users = new BK_Db_SQL($this->node, $this->query);
                $params_users['id'] = $id;
                $params_users['email'] = ($is_email) ? $username : '';
                $params_users['fname'] = (!empty($this->comm->request->post['fname'])) ? $this->comm->request->post['fname'] : '';
                $params_users['lname'] = (!empty($this->comm->request->post['lname'])) ? $this->comm->request->post['lname'] : '';
                $params_users['about'] = (!empty($this->comm->request->post['about'])) ? $this->comm->request->post['about'] : '';

                // Add to the user information table
                if ($sql_users->create($params_users)) {
                    // Commit tran to write to db
                    $this->query->commitTran();
                } else {
                    // Rollback transaction, something went wrong
                    $this->query->rollbackTran();  
                    throw new Exception("Failed to create user final", 400);
                }

            } else {
                // Rollback transaction, something went wrong
                $this->query->rollbackTran();  
                throw new Exception("Failed to create user", 400);
            }

        } else {
            throw new Exception("Validation error", 400);
        }

        // We don't want to return the id so remove it from array
        unset($params_users['id']);

        // Return the user information entered
        return array("data" => $params_users, "meta" =>  new stdClass);
    }

}