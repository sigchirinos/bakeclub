({
    appDir: "./",
    baseUrl: "lib",
    dir: "../build",
    //Comment out the optimize line if you want
    //the code minified by UglifyJS
    //optimize: "none",

    paths: {
        main: '../main',
        app: '../app',
        backbone: 'backbone',
        bootstrap: 'bootstrap',
        cubiq: 'cubiq'
    },
    // Shim config for js libraries that don't use "define"
    shim: {
        'backbone/underscore': {
            exports: "_"
        },
        'backbone/backbone': {
            deps: ['backbone/underscore'],
            exports: 'Backbone'
        },
        'cubiq/iscroll': {
            exports: "iScroll"
        },
        'bootstrap/bootstrap': {}
    },

    modules: [
        //Optimize the application files. jQuery is not 
        //included since it is already in require-jquery.js
        {
            name: "main"
        }
    ]
})
