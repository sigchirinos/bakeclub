<?php
// Register autoloader for BAKE framework
spl_autoload_register(function ($className) {

    // Convert undersctore to forward slashes
    $class_path = str_replace("_", DIRECTORY_SEPARATOR, $className);
    
    // Add file extension 
    $class_path = $class_path.".class.php";
    
    // Check for existence of file
    require $class_path;

}); // end function

// Init Slim framework
require '../lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/**
 * The class is a global configuration settings container for all APIs.
 * You could also defined api specific configuration files within each API's folder.
 */
class BK_Config {
        /**
     * Database connection information
     * @var array
     */
    public static $DB = array(
        "bake-db1" => array(
            "host"              => "localhost", 
            "dbname"    => "bakeclub", 
            "user"              => "baker", 
            "password"  => "bakeclub"
        )   
    );

    /**
     * Filesystem and other path config info
     * @var array
     */
    public static $PATH = array(
      "upload-dir" => "/Users/sig/projects/bakeclub/website/public/img/",
      "base-dir" => "/Users/sig/projects/bakeclub/website"
    );

    /**
     * Filesystem and other path config info
     * @var string
     */
    public static $LIBPATH = '/Users/sig/projects/bakeclub/website/lib';

    /**
     * Path to lgos
     * @var array
     */
    public static $LOGPATH = '/Users/sig/projects/bakeclub/logs/';

    /**
     * Key/session expiration in seconds
     * @var int
     */
    public static $KEYEXP = 21600;
}


// Add lib path to include path
ini_set('include_path', ini_get('include_path').":".BK_Config::$LIBPATH);