<?php 
// Bootstrap application
require 'bootstrap.php';

// We use this object later as route middleware
// to implement roles based access control
$acl = new \Bake\Acl();

// Create a new user
$app->get('/events', function () use ($app) {

	// Init event object
	$event = new \Bake\Event();

	// Get list of current events
	$result = $event->getCurrentEvents();

	// Send user info back to user
	echo json_encode($result);
});

// Start application
$app->run();

// Route map controls uri mapping to api function calls, as well as permissions
array(
        '@^/events/([0-9]+)$@'   => array("GET"      => "read", 
                                         "PUT"      => "update", 
                                         "DELETE"   => "delete"),

        '@/events/current$@'     => array("GET"      => "getCurrentEvents"),
        '@/events$@'             => array("POST"     => "create",
                                         "GET"      => "getEvents")
    );
