<?php 
/**
 * Media API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Media_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "media";
    
    /**
     * The table where tags are stored
     * @var string
     */
    public $tagNode = "media_tags";
    
    /**
     * Allowed fields for user requests
     * @var array
     */
    public $fields = array("user_id", "type", "type_ent_id", "caption", "path", "path_hires", "path_thumb", "tag");
    
    /**
     * Get a list of recipes
     * 
     * @return mixed
     */
    public function addMedia() {
      
      // try writing media to location
      try {
         $location = $this->writeMedia($this->comm->request->files['media']);
      } catch (Exception $e) {
        throw new Exception("Media storage failure", 500);
      }
      
      // HACK: Shouldn't modify the post array here
      // should instead manually run create media function and not the parent one 
      $this->comm->request->post['path'] = $location;
      
      // create base media
      $result = parent::create(); 
      
      return $result;
    }
    
    /**
     * Get a piece of media
     * 
     * @return mixed
     */
    public function getMedia($id) {
      
      $result = parent::read($id);
      
      // convert absolute path into uri before returning result
      if ($result['data']['path']) {
        $result['data']['path'] = str_replace(BK_Config::$PATH['base-dir'], "", $result['data']['path']);
      }
      
      return $result;
    }
    
    /**
     * Get tags for a piece of media
     * 
     * @return mixed
     */
    public function getMediaTags($id) {
       // Setup query connection
      $sql = new BK_Db_SQL($this->tagNode, $this->query);
      
      // Filter fields returned to user
      if (!empty($this->comm->request->get['fields']) && is_array($this->comm->request->get['fields'])) {
        $sql->fields = $this->filterFields($this->comm->request->get['fields']);
      }
      
      // Query DB
      if ($sql->read(array('media_id' => $id))) {
        $result = array("data" => $sql->rows, "meta" => new stdClass);
      } else {
        throw new Exception('Tags not found', 500);
      }
      
      return $result;
      
    }
    
    /**
     * Get all media locations for an event
     * 
     * @return mixed
     */
    public function getMediaByEvent($id) {
      
      // Setup query connection
      $sql = new BK_Db_SQL($this->node, $this->query);
      
      // Check filter fields 
      $sql->fields = ($this->comm->request->get['fields']) ? $this->comm->request->get['fields'] : array('*');
       
      // Query DB
      if ($sql->read(array('type' => "event", 'type_ent_id' => $id))) {
        $result = array("data" => $sql->rows, "meta" => new stdClass);
      } else {
        throw new Exception('Read failed');
      }
      
      return $result;
      
    }
    
    
    /**
     * Write media to file
     * 
     * @return mixed
     */
    protected function writeMedia($file) {
      
      if (isset($file)) {
        
        // TODO need to create util function to write files
        // set base upload dir
        $upload_dir = BK_Config::$PATH['upload-dir'];
        $upload_file = $upload_dir . basename($file['name']);
        
        // Move file from temp location to main folder
        if (move_uploaded_file($file['tmp_name'], $upload_file)) {
          return $upload_file;
        } else {
          $this->log->logFatal("Failed to write file");
          throw new Exception("Failed to write file");
        }
        
      } else {
        $this->log->logFatal("No file specified");
        throw new Exception("No file specified");
      }
    }

    /**
     * Get the most recent media
     * 
     * @return array
     */
    public function getRecentMedia() {
      return array('data' => array("title" => 'Most recent media'), 'meta' => new stdClass);
    }
     
}