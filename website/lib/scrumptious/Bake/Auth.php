<?php 
/**
 * User authentication and and role retrieval
 * @package Bake
 * @author Sigfrido Chirinos
 */
namespace Bake;

class Auth {
    
    /**
     * The database abstraction object to query a database
     * @var mixed
     */
    public $orm;

    /**
     * Object settings
     * source - [db|ldap] The authentication source type 
     * @var array
     */
    public $settings = array();

    /**
     * Constructor
     * @param string $settings object settings
     */
    public function __construct($settings = array()) 
    {
        // Overwrite default settings
        $this->settings = array_merge([
            'orm' => \ORM::for_table('auth'),
            'source' => 'db'
            ], $settings
        );

        // connection to default database
        $this->orm = $this->settings['orm'];
    }

    /**
     * Authenticates a user and stores their role information in the session
     * @param string $username the username to authenticate
     * @param string $password the password to authenticate
     * @return array
     */
    public function authenticate($username, $password) 
    {
        // Username may be email address so try validating that first
        $is_email = filter_var($username, FILTER_VALIDATE_EMAIL);

        // If failed email validation then try regular username
        if ($is_email === false) {
            $username = filter_var($username, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9\.\_]+$/")));
        }
        
        // Did username and password check out
        if ($username !== false && $password !== false) {
            // Query db for user info or query ldap
            if ($this->settings['source'] === 'ldap') {
                
                // Try authenticating against ldap
                if ($this->authLdap($username, $password)) {

                    // Retrieve and store role info in session
                    $this->getRoles($username);

                } else {
                    throw new \Bake\AppException("Incorrect username or password ldap", 400);  
                }

            } else {
                // Default to database
                $result = $this->orm->where('username', $username)->find_one();
                
                // Get authentication info for the username passed
                if ($result !== false) {
                    
                    // Check if password matches stored
                    if (\Bake\Bcrypt::check($password, $result['password'])) {

                        // Get a role info for user and store in session
                        $this->getRoles($result['id']);

                    } else {
                        throw new \Bake\AppException("Incorrect username or password", 400);
                    }

                } else {
                    throw new \Bake\AppException("Incorrect username or password", 400);
                }
            }

        } else {
            throw new \Bake\AppException("Incorrect username or password", 400);
        }

        // No exceptions thrown! Everything is ok!
        return array("data" => array('roles' => (isset($_SESSION['roles']) ? $_SESSION['roles'] : '')), "meta" => new \stdClass);
    }

    /**
     * Create a new authentication record
     * @param string $username the username 
     * @param string $password the password
     * @param string $role the role to associate with the authentication record
     * @return boolean
     */
    public function createAuthRecord($username, $password, $role = "user") 
    {
        // Try validating username as an email first
        $is_email = filter_var($username, FILTER_VALIDATE_EMAIL);

        // If failed email validation then try regular username
        if ($is_email === false) {
            $username = filter_var($username, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9\.\_]+$/")));
        }

        // TODO: Validate role
        

        // Did username and password check out
        if ($username !== false && !empty($password)) {

            // Create new authentication record
            $this->orm->create();
            $this->orm->username = $username;
            $this->orm->password = \Bake\Bcrypt::hash($password);
            $this->orm->level = $role;

            // Try to save back to db
            $this->orm->save();
                
            // Get the id from the insert
            $id = $this->orm->id();   
        } else {
            var_dump($username, $password);
            throw new \Bake\AppException("Validation error", 400);
        }

        // Return the user information entered
        return array("data" => ['id' => $id], "meta" =>  new \stdClass);
    }

    /**
     * Refresh role information into session
     * @param string $id the apikey to get role info for
     * @return array
     */
    public function refreshRoles($id) 
    {

        // Query apikeys table to get the user_id that belongs to this apikey
        $result = \ORM::for_table('apikeys')->where('id', $id)->find_one();

        // Query for role information based on user_id returned
        if ($result !== false) { 
            // Use the user id to get roles
            $this->getRoles($result['user_id']); 
        } else {
            throw new Exception("Invalid apikey", 400);
        }

        return array("data" => array('roles' => (isset($_SESSION['roles']) ? $_SESSION['roles'] : '')), "meta" => new \stdClass);
    }

    /**
     * Attempts to authenticate a by binding against ldap database
     * @param string $username ldap user id
     * @param string $password ldap password
     * @return boolean
     */
    private function authLdap($username, $password) 
    {
        // Setup ldap connection info
        $ldap_server = "ldaps://vldap.prvt.nytimes.com:636";
        $group_dn = "ou=group,dc=prvt,dc=nytimes,dc=com";
        $user_dn = "ou=People,dc=prvt,dc=nytimes,dc=com";

        // connect to server
        $conn = ldap_connect($ldap_server);
        
        // Did we connect ok?
        if ($conn === false) {
            return false;
        }

        // Bind with username and password, we need to supress any warnings
        // from this call using "@"" or it messes up the json response
        // we still check the return value through
        $bind = @ldap_bind($conn, 'uid='.$username.','.$user_dn, $password); 

        // Close connection, since we don't need it for anything else
        ldap_close($conn);

        // Check if bind was successful
        if (!$bind) {
            return false;
        }

        // Success!
        return true;
    }

    /**
     * Get a user's roles from the database
     * @param string $userId user id to get role info for
     * @return void
     */
    private function getRoles($userId) 
    {
        // Try getting role info
        $result = $this->orm->where('id', $userId)->find_one();

        // Did we get the info?
        if ($result !== false) {
            $role = $result['level'];
            $_SESSION['roles'] = array($role);
            $_SESSION['user_id'] = $result['id'];
        }
    }
     
}