<?php 
/**
 * Notes API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Notes_Api extends BK_Svc_Base {
    
    public $stampOnUpdate = true;
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "notes";
    
    /**
     * The table where tags are stored
     * @var string
     */
    public $tagNode = "note_tags";
    
    /**
     * Add a note
     * 
     * @return mixed
     */
    public function addNote() {
      // create base note
      parent::create();
      
      // check passed communication flags and
        // send to twitter acount
        // send to email system
        // send to fb page
      
    }
    
    /**
     * Get Notes
     * 
     * @return mixed
     */
    public function getNotes() {
      // Setup data connection
      $sql = new BK_Db_SQL($this->node, $this->query);
      
      // Build custom query
      $query_text = 'select * from notes';
      
      if ($sql->readCustom($query_text)) {
        $result = array("data" => $sql->rows, "meta" => new stdClass);
      } else {
        throw new Exception("Notes not found", 500);
      }
      
       return $result;
    }
    
    /**
     * Get Notes for a specific event
     * 
     * @return mixed
     */
    public function getEventNotes($id) {
      // Setup data connection
      $sql = new BK_Db_SQL($this->node, $this->query);
      
      // Build custom query
      $query_text = "select * from notes where type = 'event' and type_ent_id = ?";

      $params['type_ent_id'] = $id;
      
      if ($sql->readCustom($query_text, $params)) {
        $result = array("data" => $sql->rows, "meta" => new stdClass);
      } else {
        throw new Exception("Notes not found", 500);
      }
      
       return $result;
    }

    /**
     * Get the most recent tweets
     * 
     * @return array
     */
    public function getRecentTweets() {
      return array('data' => array("title" => 'Most recent tweets'), 'meta' => new stdClass);
    }
     
}