/**
    Mini indicator that shows the slider position in a group of slides
    Can also be used as a calendar-like view.
    @module app/views/slider/indicator
    @extends app/views/base
 */
define([
        'app/views/base',
        'text!app/tpl/slider/indicator.html'
        ],
function(BaseView, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias app/views/slider/indicator
     */
    var exports = BaseView.extend({
        /**
         * CSS class for the view's root element
         * @type {String}
         * @default
         */
        className : 'indicator',

        /**
         * Reference to the slider this indicator is attached to
         * @type {app/views/slider/slider}
         */
        slider : {},

        /**
         * Flag to determine how to display the indicator.
         * @type {String}
         * @default
         */
        mode : 'calendar',

        /**
         * Map of all event handlers for the view
         * @type {Object}
         */
        events : {
            'click .indicator a.next' : 'next',
            'click .indicator a.prev' : 'prev',
            'click .indicator ul li' : 'gotoPage'
        },

        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);
        },

        /**
         * Add a slide indicator position
         * @param {Int} slidePos the slide position this indicator is attached to
         * @params {Int} id The event id of the position. This is the id for an event returned from the API.
         * @returns this
         */
        add : function(slidePos, item) {

            // create li element
            var elem = this.make('li');

            // Set custom attributes
            $(elem).attr('slide-pos', slidePos);
            $(elem).attr('event-id', item.get("id"));

            // Date element
            //var elem_date = this.make('span');
            //$(elem_date).html(item.get("event_date"));
            //$(elem_date).addClass('date');
            //$(elem).append(elem_date);

            // Add thumbnail
            var thumb = this.make('div');
            $(thumb).addClass("thumb");
            $(thumb).css('background-image', 'url("' + item.get("path") + '")');
            $(elem).append(thumb);

            // If calendar mode is set, we need to fill in the days in between
            // for the large calendar view
            if (this.mode === 'calendar') {

            }

            // append indicator
            $('ul', this.$el).append(elem);

            // make first li active
            $('ul li', this.$el).first().addClass('active');

            return this;
        },

        /**
         * Move to next slider position
         * @param {DOM Event} event The event object for the fired event.
         */
        next : function(event) {
            // Capture default
            event.preventDefault();

            // go to next slide
            this.slider.scrollToPage('next', 0);
        },

        /**
         * Move to previus slider position
         * @param {DOM Event} event The event object for the fired event.
         */
        prev : function(event) {
            // Capture default
            event.preventDefault();

            // go to next slide
            this.slider.scrollToPage('prev', 0);
        },

        /**
         * Go to specific slide
         * @param {DOM Event} event The event object for the fired event.
         */
        gotoPage : function(event) {
            // We need to find the li
            var target_elem;
            if (event.target.tagName === 'li') {
                target_elem = event.target;
            } else {
                target_elem = event.target.parentNode;
            }
            
            // Go to specific slide, get slide position from attribute on clicked element set on creation
            this.slider.scrollToPage(parseInt($(target_elem).attr('slide-pos'), 10), 0);
        }
    });

    return exports;
});