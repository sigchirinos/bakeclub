define(['backbone/backbone'], function(Backbone){
	/**
	 * Notes
	 */
	var Note = Backbone.Model.extend({
		type: 'note',

		url: function () {
			return '/api/notes/'+this.id;
		},

	    parse : function(response) {
	        // Check for "data" property, means we are getting it back directly from server
	        if(response.data) {
	            var data = response.data;
	        } else {
	            var data = response;
	        }

	        // Create client unique identifier
	        data.client_ident = "note" + data.id;

	        return data;
	    }
	});

	return Note;
});