/**
    Footer view
    @module app/views/footer
    @extends app/views/base
 */
define([
    'app/views/base',
    'text!app/tpl/footer.html'
    ],
function(BaseView, Tpl){
    /**
     * Contructor
     * @constructor
     * @alias app/views/footer
     */
    var exports = BaseView.extend({
        /**
         * Automatically called upon object construction
         */
        initialize : function() {
            // Compile template
            this.template = _.template(Tpl);
        }
    });

    return exports;
});