<?php 
/**
 * Auth API
 *
 * @package Bake
 * @author Sigfrido Chirinos
 */
class BK_Svc_Auth_Api extends BK_Svc_Base {
    
    /**
     * The base table/node that stores data for this service
     * @var string
     */
    public $node = "auth";

    /**
     * What to use as the authentication source, ie: database, ldap, etc...
     * @var string
     */
    public $source = "db";

    /**
     * Authenticate a user and set their role information
     * 
     * @return boolean
     */
    public function authenticate() 
    {
        // Username may be email address so try validating that first
        $username = filter_var($this->comm->request->post['username'], FILTER_VALIDATE_EMAIL);

        // If failed email validation then try regular username
        if ($username === false) {
            $username = filter_var($this->comm->request->post['username'], FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[A-Za-z0-9\.\_]+$/")));
        }

        // TODO: filter password with greater range of options maybe
        $password = $this->comm->request->post['password'];

        // Did username and password check out
        if ($username !== false && $password !== false) {
            // Query db for user info or query ldap
            if ($this->source === 'ldap') {
                
                // Try authenticating against ldap
                if ($this->authLdap($username, $password)) {

                    // Retrieve and store role info in session
                    $this->getRoles($username);

                } else {
                    throw new Exception("Incorrect username or password ldap", 400);  
                }

            } else {
                // Default to database
                // Setup data connection
                $sql = new BK_Db_SQL($this->node, $this->query);

                // Get authentication info for the username passed
                if ($sql->read(array('username' => $username))) {
                    
                    // Check if password matches stored
                    if (BK_Util_Bcrypt::check($password, $sql->rows[0]['password'])) {

                        // Get a role info for user and store in session
                        $this->getRoles($username);

                    } else {
                        throw new Exception("Incorrect username or password", 400);
                    }

                } else {
                    throw new Exception("Incorrect username or password", 400);
                }
            }

        } else {
            throw new Exception("Incorrect username or password", 400);
        }

        // No exceptions thrown! Everything is ok!
        return array("data" => array('roles' => (isset($_SESSION['roles']) ? $_SESSION['roles'] : '')), "meta" => new stdClass);
    }

    /**
     * Refresh role information into session
     * @return array
     */
    public function refreshRoles() 
    {
        // Query db for user_id based on apikey
        $id = session_id();

        // Setup data connection
        $sql = new BK_Db_SQL('apikeys', $this->query);

        // Query for role information based on user_id returned
        if ($sql->read(array('id' => $id))) { 
            // Store user_id belonging to this sesson
            $user_id = $sql->rows[0]['user_id'];

            // Query auth table for role info
            $sql = new BK_Db_SQL($this->node, $this->query);

            // Get role info for the user
            if ($sql->read(array('id' => $user_id))) {
                $role = $sql->rows[0]['level'];
                $_SESSION['roles'] = array($role => true);
                $_SESSION['user_id'] = $sql->rows[0]['id'];
            } 

        } else {
            throw new Exception("Invalid apikey", 400);
        }

        return array("data" => array('roles' => (isset($_SESSION['roles']) ? $_SESSION['roles'] : '')), "meta" => new stdClass);
    }

    /**
     * Authenticate a user against ldap
     * @param string $username ldap user id
     * @param string $password ldap password
     * @return boolean
     */
    private function authLdap($username, $password) 
    {
        // Setup ldap connection info
        $ldap_server = "ldaps://vldap.prvt.nytimes.com:636";
        $group_dn = "ou=group,dc=prvt,dc=nytimes,dc=com";
        $user_dn = "ou=People,dc=prvt,dc=nytimes,dc=com";

        // connect to server
        $conn = ldap_connect($ldap_server);
        
        // Did we connect ok?
        if ($conn === false) {
            return false;
        }

        // Bind with username and password, we need to supress any warnings
        // from this call using "@"" or it messes up the json response
        // we still check the return value through
        $bind = @ldap_bind($conn, 'uid='.$username.','.$user_dn, $password); 

        // Close connection, since we don't need it for anything else
        ldap_close($conn);

        // Check if bind was successful
        if (!$bind) {
            return false;
        }

        // Success!
        return true;
    }

    /**
     * Get a user's roles
     * @param string $username username to get role info for
     * @return void
     */
    private function getRoles($username) 
    {
        // Setup data connection
        $sql = new BK_Db_SQL($this->node, $this->query);

        // Try getting role info
        if ($sql->read(array('username' => $username))) {
            $role = $sql->rows[0]['level'];
            $_SESSION['roles'] = array($role => true);
            $_SESSION['user_id'] = $sql->rows[0]['id'];
        }
    }
     
}